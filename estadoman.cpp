#include "estadoman.h"
#include <string.h>

EstadoMan::Estado::Estado(istream * obj, const uint64_t id, const string nombre, const unsigned tamDisp) {
	this->disparador = (char*)malloc(tamDisp);
	memset(this->disparador, 0x00, tamDisp);
	obj->read((char*)this->disparador, tamDisp);
	this->id = id;
	this->nombre = nombre;
	this->lenDisparador = tamDisp;
	delete obj;
}

EstadoMan::Estado::~Estado() {
	free(this->disparador);
}

EstadoMan::EstadoMan(const string protocolo, DBConexion * db) {
	//Leer de la base de datos la lista de estados que aplican a este protocolo.
	stringstream qry;
	this->dbcon = db;
	qry << "select disparador, idestado, nombre, length(disparador) from vw_estado where idprotocolo = '"<< protocolo << "';";
	ResultSet *rs = this->dbcon->select(qry.str());
	if (rs != 0L) {
		while(rs->next()) {
			this->estados.push_back(new Estado(rs->getBlob(1), rs->getUInt64(2), rs->getString(3), rs->getUInt(4)));
		}
	}
	delete rs;
}

EstadoMan::Estado * EstadoMan::busca(const char* e, const unsigned tam) {
	for (unsigned i = 0; i < this->estados.size(); i++) {
		if (this->estados.at(i)->lenDisparador == tam && memcmp(this->estados.at(i)->disparador, e, this->estados.at(i)->lenDisparador) == 0)
			return this->estados.at(i);
	}
	return 0L;
}

EstadoMan::Estado * EstadoMan::busca(const char e) {
	for (unsigned i = 0; i < this->estados.size(); i++) {
		if (this->estados.at(i)->disparador[0] == e) return this->estados.at(i);
	}
	return 0L;
}

uint64_t EstadoMan::acumula(const char e) {
	uint64_t retval = 0;
	for (unsigned i = 0; i < this->estados.size(); i++) {
		if ((this->estados.at(i)->disparador[0] & e) == this->estados.at(i)->disparador[0]) retval |= this->estados.at(i)->id;
	}
	return retval;
}

uint64_t EstadoMan::acumula(const char* e, const unsigned int tam) {
	uint64_t retval = 0;
	for (unsigned i = 0; i < this->estados.size(); i++) {
		if (this->estados.at(i)->lenDisparador == tam && compara(e, this->estados.at(i)->disparador, tam))
			retval |= this->estados.at(i)->id;
	}
	return retval;
}

bool EstadoMan::compara(const char* e, const char* d, const unsigned int tam) {
	bool retval = true;
	for (unsigned i = 0; i < tam; i++) {
		retval = (retval && ((e[i] & d[i]) == d[i]));
	}
	return retval;
}

EstadoMan::~EstadoMan() {
	for (unsigned i = 0; i < this->estados.size(); i++) {
		delete this->estados.at(i);
	}
}
