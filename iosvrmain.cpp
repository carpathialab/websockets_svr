#include "iosvrmain.h"

//Constantes de servidor:
const int EN_ESPERA = 5;	//Conexiones que pueden estar esperando a conectar

//Variables de servidor, que se pueden ajustar desde la configuración:
string puerto = "5000";
unsigned maxUsuarios = 200;
unsigned conectados = 0;
unsigned tcpverifica = 10;
bool fifando = true;
bool verboso = false;
bool pingea = true;
vector< Protocolo* > misProtocolos;

//Variables para los Threads...
volatile fd_set the_state;

pthread_mutex_t mutex_state = PTHREAD_MUTEX_INITIALIZER;

//Fareden Hacks y estructuras:
IOConfigurador	* miconf;
IOLogger		* milog;
Usuarios		losUsers;
//Base de datos

//pthread_mutex_t boardmutex = PTHREAD_MUTEX_INITIALIZER; // mutex locker for the chessboard vector.
int main(int argc, char **argv) {
	bool modoX = false;
	Servidor::InfoDebug dbg;
	//Validamos que sea instancia única:
	svrnombre = string(argv[0]);
	svrnombre = svrnombre.substr(svrnombre.find_last_of("/") + 1);
	pid = "/var/log/ixpiyani/" + svrnombre + ".pid";
	int elPid = open(pid.c_str(), O_CREAT | O_RDWR, 0666);
	int ctrl = flock(elPid, LOCK_EX | LOCK_NB);
	if (ctrl) {
		if (EWOULDBLOCK == errno) {
			cerr << "Saliendo precipitadamennte del proceso por culpa de la escasés de rinocerontes\n";
			return 3;
		}
	}
	//Iniciando los interceptores de señal...
	signal(SIGSEGV, manejaSignals);
	signal(SIGTERM, manejaSignals);
	signal(SIGABRT, manejaSignals);
	signal(SIGINT, manejaSignals);
	signal(SIGUSR1, manejaSignals);
	signal(SIGKILL, manejaSignals);
	string arConf = "/etc/" + svrnombre + ".conf";
	dbg.regInit = 0;
	dbg.regFin = 0;
	dbg.dbAplica = false;
	dbg.hazPausas = false;
	terminando = false;
	edoMan = 0L;
	dbman = 0L;
	dbg.leidoEstado = 't';
	erroresLleno = 0;
// 	cout << "Argumento 0: " << argv[0] << std::endl;
	for (unsigned ar = 1; ar < argc; ar++) {
		switch(argv[ar][1]) {
			case 'c':
				//Es la configuración...
				arConf = argv[++ar];
				break;
			case 'v':
				cout << "Activando modo platicador...\n";
				verboso = true;
				break;
			case 'h':
				cout << "Modo de uso: ionetserver -v -c [archivo de configuración]\n";
				cout << "\t-v: activa modo expresivo en stdout\n\t-c [archivo de configuración]: usa un archivo de configuración distinto a /etc/ionetserver.conf\n";
				return 0;
				break;
			case 'i':	//Registro inicial
				dbg.regInit = atol(argv[++ar]);
				break;
			case 'f':
				dbg.regFin = atol(argv[++ar]);
				break;
			case 'e':
				dbg.dbAplica = true;
				break;
			case 'p':
				dbg.hazPausas = true;
				break;
			case 'm':
				dbg.imeiDebug = argv[++ar];
				break;
			case 'l':
				dbg.leidoEstado = argv[++ar][0];
				break;
			case 'x':
				//Funciones experimentales... debería estar apagdo siempre...
				modoX = true;
				break;
		}
	}
	miconf = new IOConfigurador(arConf);
	puerto = miconf->leeValor("puerto");
	maxUsuarios = atoi(miconf->leeValor("usuarios").c_str());
	milog = new IOLogger(svrnombre, miconf->leeValor("rutalog"), 7700, 9, atoi(miconf->leeValor("nivel").c_str()));
	milog->iniciaLog();
	milog->setVerbosidad(verboso);
	OpcionesTCP opts_tcp;
	dbman = new DBConexion(miconf->leeValor("dbsvr"), miconf->leeValor("dbusr"), miconf->leeValor("dbpwd"), miconf->leeValor("dbnombre"));
	if (!dbman->conectado) {
		milog->println("No hay conexión a la base de datos", 4);
		return 1;
	} else if (modoX) {
		//TODO Aquí van las funciones muy experimentales...
		;
	} else {
		opts_tcp.errsock = miconf->leeValor("erroressocket");
		opts_tcp.kaopt = miconf->leeValor("intervalokeepalive");
		opts_tcp.soka = miconf->leeValor("keepalive");
		opts_tcp.soto = miconf->leeValor("enviotimeout");
		opts_tcp.slin = miconf->leeValor("linger");
		opts_tcp.slin2 = miconf->leeValor("linger2");
		opts_tcp.skepid = miconf->leeValor("inactividadka");
		Mitotiani * mitote = new Mitotiani(miconf->leeValor("svrinterno"), miconf->leeValor("ptointerno"), milog, (dbg.regInit != 0), svrnombre, &opts_tcp);
#ifdef _WEBSOCKETSSVR_
		Servidor * principal = new Servidor(puerto, maxUsuarios, mitote, milog, dbman, USR_WEBSOCKET, svrnombre, &dbg);
#else
		Servidor * principal = new Servidor(puerto, maxUsuarios, mitote, milog, dbman, USR_PROTOCOLO, svrnombre, &dbg);
#endif
		if (principal->preparaEscucha(&opts_tcp)) {
			principal->inicia();
		} else {
			return 2;
		}
		
/*
 *void Servidor::ajustaOpciones(const std::__cxx11::string es, const std::__cxx11::string ko, const std::__cxx11::string sk, const std::__cxx11::string st, const std::__cxx11::string sl, const std::__cxx11::string sl2, const std::__cxx11::string skid) {
	this->errsock = es;
	this->kaopt = ko;
	this->soka = sk;
	this->soto = st;
	this->slin = sl;
	this->slin2 = sl2;
	this->skepid = skid;
}
*/		
#ifdef _WEBSOCKETSSVR_
		dbman->ejecutaSQL("truncate websocket;");
		Servidor * interno = new Servidor("5600", maxUsuarios, mitote, milog, dbman, USR_INTERNO, svrnombre + "_int", &dbg);
		if (interno->preparaEscucha(&opts_tcp)) {
			interno->inicia();
		} else {
			return 3;
		}
		if (interno->hilo->joinable()) {
			cout << "Vamos a poner la traba\n";
			interno->hilo->join();
		}
#endif
		if (principal->hilo->joinable()) {
			cout << "Vamos a esperar el principal\n";
			principal->hilo->join();
		}
	}
	return 0;
}
//ATENCIÓN, esta función puede cambiar mucho e idealmente nunca debe de aparecer en los commits sin estar comentada...
void hazMagia() {
	stringstream qry;
	qry << "select count(*) from crudo where protocolo = 'gt06e.ix' and leido = '0' and rayo is not null and rayo > 0 order by rayo;";
	long long total = 0;
	long long pagina = 0;
	ResultSet *rs = dbman->select(qry.str());
	if (rs != 0L) {
		if (rs->next()) {
			total = rs->getUInt64(1);
			cout << "Vamos a trabajar con " << total << " registros...\n";
		} else {
			return;
		}
	} else {
		return;
	}
	for (pagina = 10000; pagina < total; pagina += (pagina + 10000 > total ? total - pagina : 10000)) {
		qry.str("");
		qry << "select id, rayo, paquete, length(paquete) from crudo where protocolo = 'gt06e.ix' and leido = '0' and rayo is not null and rayo > 0 order by rayo limit 10000," << pagina << ";";
		ResultSet *rs = dbman->select(qry.str());
		if (rs != 0L) {
			cerr << "Procesando página: " << pagina << std::endl;
			long long rayoAnt = 0;
			bool paso = false, esfin = false;
			char buff[4800];
			char paq[4800];
			int pos = 0;
			int tamDebug = 0;
			long long ancla = 0;
			while (rs->next()) {
				long long rayo = rs->getInt64(2);
				if (rayoAnt != rayo) {
					if (pos > 0) {
						cout << "Cambiando al rayo: " << rayo << std::endl;
						//Hacer el procesamiento del buffer...
						unsigned bpos = 0;
						ancla = 0;
						for (unsigned i = 0; i < pos; i++) {
							if (!paso && ((buff[i] == 0x78 && buff[i + 1] == 0x78) || (buff[i] == 0x79 && buff[i + 1] == 0x79))) {
								paso = true;
								esfin = false;
							} else if (paso && (buff[i] == 0x0D && buff[i + 1] == 0x0A)) {
								paso = false;
								esfin = true;
								//Agregamos el fin al paquete...
								paq[bpos++] = 0x0D;
								paq[bpos++] = 0x0A;
								i++;
							}
							if (paso) {
								paq[bpos++] = buff[i];
							}
							if (esfin) {
								stringstream qry2;
								ancla = rs->getUInt64(1);
								qry2 << "update crudo set paquete = ?, leido = '2' where id = '"<< ancla << "';";
								dbman->ejecutaBinario(qry2.str(), (unsigned char *)paq, bpos);
								//cout << qry2.str() << std::endl;
							}
						}
						if (ancla != 0) {
							cout << "delete from crudo where rayo = '" << rayoAnt << "' and id != '" << ancla << "';\n";
						}
					}
					pos = 0;
					rayoAnt = rayo;
					memset(buff, 0x00, 4800);
					memset(paq, 0x00, 4800);
				} else {
// 					cout << "Procesando paquete: " << rs->getUInt64(1) << std::endl;
					istream *tmp = rs->getBlob(3);
					tamDebug = rs->getUInt(4);
					if (pos + tamDebug < 4800) tmp->read(buff + pos, tamDebug);
					pos += tamDebug;
				}
				//vamos a buscar un inicio de paquete...
			}
		} else {
			cerr << "Falló la consulta...\n";
		}
	}
}

// int envia(int fd, string datos) {
// 	datos +="\r";
// 	return send(fd, datos.c_str(), strlen(datos.c_str()),0);
// }
// 
// int envia(string cliente, string datos) {
// 	int cliDest = atoi(cliente.c_str());
// 	int retval ;
// 	if (cliDest > -1) {
// 		retval = envia(losUsers.getCliente(atoi(cliente.c_str()))->getFD(), datos);
// 	} else {
// 		char reporte[100];
// 		sprintf(reporte, "No puedo enviar el mensaje a un destino negativo: %d", cliDest);
// 		if (verboso) cout << reporte << "\n";
// 		milog->println(string(reporte), (errno != 0 ? 2 : 4));
// 		retval = -1;
// 	}
// 	return retval;
// }
// 
// void registraNoProc(const string mensaje, const vector< string > partes) {
// 	milog->println("Comando no procesado: " + mensaje + " --partes:", 4);
// 	for (unsigned i = 0; i < partes.size(); i++)
// 		milog->println(partes[i], 4);
// }

/**
 * Aquí está el ciclo principal, aquí se procesa toda la información y es un thread independiente...
 */
void manejaSignals(int numSignal) {
	switch (numSignal) {
		case SIGTERM: {
			milog->println("*** Señal de terminación recibida!! ***", 1);
			if (dbman != 0L) dbman->termina();
			milog->terminaLog();
			exit(numSignal);
		}
		break;
		case SIGINT: {
			char sale;
			cerr << "¿Realmente desea salir? terminará todas las conexiones activas... (S/n): "; cin >> sale;
			if (sale == 's' || sale == 'S') {
				milog->println("*** Terminado por el usuario usando Control + C ***", 1);
				if (dbman != 0L) dbman->termina();
				milog->terminaLog();
				exit(numSignal);
			} else {
				milog->println("Salida cancelada...", 4);
				cerr << "... seguimos trabajando, pues...\n";
			}
		} break;
		case SIGKILL: {
			milog->println("*** Proceso matado desde el OS ***", 1);
			if (dbman != 0L) dbman->termina();
			milog->terminaLog();
			//exit(numSignal);
		} break;
		case SIGABRT:
		case SIGSEGV: {
			if (!terminando) {
				terminando = true;
				void	*arreglo[30];
				size_t	tam;
				tam = backtrace(arreglo, 30);
				
				char **traza = backtrace_symbols(arreglo, tam);
				milog->println("** Recibida la señal " + string(strsignal(numSignal)) + " **", 2);
				for (unsigned i = 0; i < tam; i++) {
					milog->println(traza[i], 2);
				}
				//Volcamos todos los logs de los protocolos antiguos:
				for (unsigned i = 0; i < losUsers.numClientes(); i++) {
					if (losUsers.at(i)->hayPendientes()) {
						milog->println(losUsers.at(i)->volcado(), 2);
					}
					delete losUsers.at(i);
				}
				if (dbman != 0L) dbman->termina();
				milog->terminaLog();
			}
			exit(numSignal);
			//throw std::invalid_argument("Error de manejo de memoria o comando mal construido");
		} break;
	}
	return;
}

//TODO Quizás sea buena idea migrarlo
// void bestias(int elFD) {
// 	//Borrar el dato de la tabla de usuarios...
// 	cout << "Usuarios antes de borrar: " << losUsers.numClientes() << std::endl;
// 	losUsers.borraUsuario(elFD);
// 	cout << "Usuarios DESPUES de borrar: " << losUsers.numClientes() << std::endl;
// 	FD_CLR(elFD, &the_state);      // free fd's from  clients
// 	close(elFD);
// 	//Reenvíar la tabla de usuarios
// }

void imprimeBufBinario(const char* buf, const unsigned int len) {
	std::cout << "0: ";
	for (unsigned c = 0; c < len; c++) {
		std::bitset<8> bs(buf[c]);
		std::cout << bs << " ";
		if ((c + 1) % 4 == 0)
			std::cout << std::endl << c << ": ";
	}
	std::cout << "\n************\n";
}
/*
void intercomm() {
	milog->println("Revisando comunicación interprocesos!", 1);
	if (dbman != 0L) {
		stringstream qry;
		qry << "select id, funcion, parametros from intercom where protocolo = '" << svrnombre << "' and fresp is null order by id asc;";
		sql::ResultSet* rs = dbman->select(qry.str());
		if (rs != 0L && rs->rowsCount() > 0) {
// 			vector < string > * params;
			while(rs->next()) {
				qry.str("");
				stringstream con;
				con << "Ejecutando mensaje de intercom: " << rs->getString(3) << ". Hay " << listaEnvios.size() << " mensajes en proceso\n";
				switch(rs->getUInt(2)) {
					case 0:
						cout << "Dice mi abuelita que " << rs->getString(3) << std::endl;
						qry << "update intercom set respuesta = 'noooo', fresp = now() where id = '" << rs->getUInt(1) << "';";
						break;
					case 1:	//Listado de clientes conectados...
						qry << "update intercom set respuesta = '" << losUsers.getUsuariosHR() << "', fresp = now() where id = '" << rs->getUInt(1) << "';";
						break;
					case 2: {
						vector < string > params = IOCore::split(rs->getString(3), "|", 3);
						if (params.size() == 3) {
							pthread_mutex_lock(&mutex_state);
							listaEnvios.push_back(new Protocolo::cmdEnviar(rs->getUInt(1), params[0], params[1], params[2], dameTS()));
							pthread_mutex_unlock(&mutex_state);
						} else {
							milog->println("Comando mal construído!", 2);
						}
					} break;
#ifdef _WEBSOCKETSSVR_
					//TODO aquí puede ir la parte donde mande las notificaciones a la API
					case 3: {
						vector < string > usrs = IOCore::split(rs->getString(3), "|");
						for (unsigned i = 0; i < usrs.size(); i++) {
							if (usrs.at(i).substr(0, 1) != "*") {
								pthread_mutex_lock(&mutex_state);
								listaEnvios.push_back(new Protocolo::cmdEnviar(rs->getUInt(1), usrs.at(i), "A", "", dameTS()));
								pthread_mutex_unlock(&mutex_state);
							} else {
								//Enviar la llamada a la API
								//TODO Necesitamos un listado con los comandos a ejecutar por API push que vayamos a implementar...
								con << "API: " << IOCore::ejecuta("achichincle") << std::endl;
							}
						}
					} break;
					case 4: {
						vector < string > usrs =IOCore::split(rs->getString(3), "|");
						for (unsigned i = 0; i < usrs.size(); i++) {
							vector < string > params = IOCore::split(usrs.at(i), ",", 2);
							pthread_mutex_lock(&mutex_state);
							listaEnvios.push_back(new Protocolo::cmdEnviar(rs->getUInt(1), params.at(0), "C|" + params.at(1), "", dameTS()));
							pthread_mutex_unlock(&mutex_state);
						}
					} break;
					case 5: {
						vector < string > partes = IOCore::split(rs->getString(3), "|", 2);
						pthread_mutex_lock(&mutex_state);
						listaEnvios.push_back(new Protocolo::cmdEnviar(rs->getUInt(1), partes.at(0), "D|" + partes.at(1), "", dameTS()));
						pthread_mutex_unlock(&mutex_state);
					} break;
#endif
				}
				
				if (qry.str() == "") qry << "delete from intercom where id = '" << rs->getUInt(1) << "';";
				con << "Consulta a ejecutar: " << qry.str();
				dbman->ejecutaSQL(qry.str());
				if (con.str() != "") {
					milog->println(con.str(), 4);
					con.str("");
				}
			}
		} else {
			milog->println("No hay registros a procesar!!", 2);
			momento_sig = dameTS();
		}
		delete rs;
	} else{
		milog->println("No hay conexión a base de datos!!" , 2);
	}
}
double dameTS() {
	time_t t = time(nullptr);
	return difftime(t, mktime(&offsetTiempo));
}
*/

