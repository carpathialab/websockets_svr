#!/bin/bash
svrnom=eco.ixpiyani
pid=$(pgrep $svrnom)
if [ -z $pid ]; then
	echo $(date)": Puso a correr el proceso por que estaba caído" >> /var/log/error_$svrnom.log
	$svrnom
else
	pcuso=$(ps -hp $pid -o %cpu)
	#echo "PC uso $pcuso"
	if (( ${pcuso%%.*} > 80 )) ; then
		echo $(date)": $pcuso El proceso se disparó, lo reinicia" >> /var/log/error_$svrnom.log
		kill -9 $pid
		$svrnom
	fi
fi
exit 0
