#!/bin/bash
cd /usr/local/bin
for i in $(ls *.ixpiyani) ; do
	pid=$(pgrep $i)
	if [ -z $pid ]; then
		echo $(date)": Puso a correr el proceso por que estaba caído" >> /var/log/error_$i.log
		$i
	else
		pcuso=$(ps -hp $pid -o %cpu)
		#echo "PC uso $pcuso"
		if (( ${pcuso%%.*} > 80 )) ; then
			echo $(date)": $pcuso El proceso se disparó, lo reinicia" >> /var/log/error_$i.log
			kill -9 $pid
			$i
		fi
	fi
done
exit 0
