#include <thread>
#include <utility> // std::move
#include <type_traits>

template<typename T>
class thread_joiner;

template<>
class thread_joiner<std::true_type> {
    explicit thread_joiner(std::thread&& t) 
        : _t(std::move(t)) {}

    // Move semantics
    thread_joiner(thread_joiner&&) = default;
    thread_joiner& operator=(thread_joiner&&) = default;

    ~thread_joiner() { if (_t.joinable()) _t.join();}

    std::thread& get() { return _t; }

private:
    std::thread _t;
};

template<>
class thread_joiner<std::false_type> {
    explicit thread_joiner(std::thread&& t) 
        : _t(std::move(t)) {}
    
    // Move semantics
    thread_joiner(thread_joiner&&) = default;
    thread_joiner& operator=(thread_joiner&&) = default;

    ~thread_joiner() { if (_t.joinable()) _t.detach();}
    std::thread& get() { return _t; }

private:
    std::thread _t;
};

using thread_join = thread_joiner<std::true_type>;
using thread_detach = thread_joiner<std::false_type>;
