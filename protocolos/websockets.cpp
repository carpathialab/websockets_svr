#include "websockets.h"

WebSockets::WebSockets(const unsigned int f, DBConexion* dbcon, IOCore::IOLogger* l, std::__cxx11::string p) : Protocolo(f, p, dbcon) {
	this->milog = l;
	this->err = 0;
	this->platicando = false;
	this->esFin = true;
	this->hayFrag = false;
	this->tamFrag = 0;
	this->tmrPing = 0;
	this->sigue = true;
}

//HERE is the cool stuff
bool WebSockets::activo() {
	bool leidoProt = false, pendiente = true;
	string mensaje;
	long carga = 0, leido = 0;
	uint8_t tCarga = 0;
	unsigned int offset = 0;
	bitset<8> mascara[4];
	if (!this->platicando) {
		do {
			this->err = leeSocket(this->buff, MAX_MENSAJE);
			//pthread_mutex_lock(&mutex_state);
			if (this->err > 0) {
				for (unsigned c = 0; c < this->err; c++) {
					mensaje += this->buff[c];
					if (this->buff[c] == '\n') {
						pendiente = !IOCore::strTerminaCon(mensaje, "\r\n\r\n");
						if (!pendiente) break;
					}
				}
			} else {
				usleep(500 * 1000);
			}
		} while (pendiente);
		if (mensaje != "") {
			vector< string > lineas = IOCore::split(mensaje, "\r\n");
			string llave = "";
			if (lineas[0].substr(0, 6) == "GET /?") {
				for (unsigned i = 1; i < lineas.size(); i++) {
					if (lineas[i] != "") {
						vector< string > par = IOCore::split(lineas[i], ": ", 2);
						if (par[0] == "Sec-WebSocket-Key") llave = par[1];
						//TODO detectar otros valores como host, etc.
						if (par[0] == "X-Forwarded-For") this->miIP = par[1];
					}
				}
				//Calculamos la nueva llave:
				//llave + 258EAFA5-E914-47DA-95CA-C5AB0DC85B11 > sha1 > base64
				llave += "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
				unsigned char tmp[20];
				memset(tmp, 0x0, 20);
				SHA1((unsigned char*)llave.c_str(), llave.length(), tmp);
				llave = IOCore::base64_encode(tmp, 20);
				//Enviamos el saludo de regreso...
				this->enviastr("HTTP/1.1 101 Web Socket Protocol Handshake Switching Protocols\r\n\
Access-Control-Allow-Credentials: true\r\n\
Access-Control-Allow-Headers: content-type\r\n\
Access-Control-Allow-Headers: authorization\r\n\
Access-Control-Allow-Headers: x-websocket-extensions\r\n\
Access-Control-Allow-Headers: x-websocket-version\r\n\
Access-Control-Allow-Headers: x-websocket-protocol\r\n\
Access-Control-Allow-Origin: http://localhost\r\n\
Connection: Upgrade\r\n\
Sec-WebSocket-Accept: " + llave + "\r\n\
Server: Ozomahtli\r\n\
Upgrade: websocket\r\n\r\n");
// 				con << "... saludó con llave: " << llave << std::endl;
				string pet = lineas[0].substr(6);
				pet = pet.substr(0, pet.length() - 9);
				//Process the web-styled request
				vector< string > arrPet = IOCore::split(pet, "&");
				for (unsigned p = 0; p < arrPet.size(); p++) {
					vector< string > prt = IOCore::split(arrPet[p], "=");
					if (prt[0] == "id") this->idEquipo = prt[1];
				}
				this->platicando = true;
				leidoProt = true;
				mensaje = "";
				con << "New client logged in successfully: " << this->idEquipo << std::endl;
				//This is optional, is just to send a flag when successfully connected
				this->enviastr("Piyalli!");
// 				free(tmp);
			} else {
				sigue = false;
			}
		} else {
			sigue = false;
		}
		con << "Greeting finished successfully\n";
		if (this->con.str() != "") {
			this->milog->println(this->con.str(), 4);
			this->con.str("");
		}
	} else {
		unsigned tipo = 0;
		//We are talking on websockets now...
		do {
			this->err = this->leeSocket(this->buff, 2);
			if (err == 2) {
				bitset<8> hdr1(this->buff[0]);
				bitset<8> hdr2(this->buff[1]);
				esFin = hdr1[7];
				this->con << "*** " << this->idEquipo << ": Es fin? " << esFin << std::endl;
				unsigned tipotmp = (bitset<8>(hdr1<<4)>>4).to_ulong();
				tipo = (hayFrag ? tipo : tipotmp);
				if (esFin && !hayFrag) {
					//RSV bits MUST be zeros
					if ((hdr1[6] || hdr1[5] || hdr1[4])) {
						con << "BITs RSV no adecuados...\n";
						sigue = false;
						break;
					}
				} else if (esFin && hayFrag && tipotmp == 0) {
					//TODO contabilizar los reserved
					//cout << "Es el fin de un paquete fragmentado: " << hdr1.to_string() << std::endl;
					hayFrag = false;
				} else if (esFin && hayFrag && tipotmp != 0) {
					con << "Weird control type... " << hdr1.to_string() << std::endl;
					esFin = false;
					continue;
				} else {
					hayFrag = true;
				}
				//Checking the message type
				if (hdr2[7]) {
					//This MUST be on when the message comes from the client side
					tCarga = (bitset<8>(hdr2<<1)>>1).to_ulong();
					switch (tCarga) {
						case 126:
							err = this->leeSocket(this->buff, 2);
							if (err == 2) {
								bitset<16> tam = this->buff[0] << 8 | this->buff[1];
								carga = (unsigned int)tam.to_ulong();
							} else {
								sigue = false;
							}
							break;
						case 127:
							err = this->leeSocket(this->buff, 8);
							if (err == 8) {
								bitset<64>tam2;
								for (unsigned i = 0; i < 8; i++) {
									tam2 |= ((uint64_t)this->buff[i]) << ((8 - (i+1)) * 8);
								}
								carga = (long)tam2.to_ulong();
							} else {
								sigue = false;
							}
							break;
						default:
							carga = tCarga;
							break;
					}
					if (hayFrag && tamFrag == 0) tamFrag = carga;
					con << "... final size " << carga << std::endl;
				} else {
					con << "HDR[2] bit 7 failed, aborting connection\n";
					sigue = false;
					break;
				}
				//Getting the binary mask...
				err = this->leeSocket(this->buff, 4);
				if (err == 4) {
					for (unsigned m = 0; m < 4; m++) {
						mascara[m] = bitset<8>(this->buff[m]);
					}
				}
				//We have the load size, let's read until is filled'
				do {
					err = this->leeSocket(this->buff, (carga - leido));
					leido += err;
					//All bytes has ben read successfully...
					con << this->idEquipo << ":\t";
					switch (tipo) {
						case 8:		//Es un error
							con << "Websockets Error:" << std::endl;
						case 3: case 4: case 5: case 6: case 7:
							con << "Control message: " << tipo << " received. Buffer size: " << err << std::endl;
						case 1:		//It's a text message'
							for (unsigned c = 0; c < err; c++) {
								bitset<8> bs(this->buff[c]);
								mensaje += (char)(bs^mascara[(c - offset) % 4]).to_ulong();
							}
							break;
						case 2:		//Binary type, I guess
							//... but, by now, we don't want it
							con << "They tried to send us a binary. Please don't..." << std::endl;
							break;
						case 9:
							con << "Ping received: " << this->miIP << std::endl;
							break;
						case 10:
							con << "Pong received: " + this->miIP << std::endl;
							break;
					}
					if (leido == carga && !hayFrag) {
						if (tipo == 1) {
							//Is a text message
						} else if (tipo == 2) {
							//This is a binary message
						} else {
							//Everything else are service messages
						}
					} else if (leido == carga && hayFrag) {
						//Qué vamos a hacer con los pendientes?
						switch (tipo) {
							case 1:
								mensaje += mensaje;
								break;
							default:
								con << "Unexpected...\n";
								break;
						}
					}
				} while (leido < carga);
			} else if (this->err < 0) {
				sleep(1);
				break;
			} else {
				con << "Connection error, closing\n";
				sigue = false;
			}
		} while (!esFin);
	}
	if (this->pausas) usleep(500 * 1000);
	if (++this->tmrPing > LANZA_PING) {
		//Sends a meaningless string just to keep the socket alive
		if (this->pingea(""))
			this->con << "buenoh... " << std::endl;
	}
	if (this->con.str() != "") {
		this->milog->println(con.str(), 4);
		this->con.str("");
	}
	//This is for debugging: stores a raw binary copy of each received chunk / message.
	this->mandaCrudo(leidoProt);
	return sigue;
}
//and HERE finishes the cool stuff
int WebSockets::enviastr(string datos) {
	int retval;
	unsigned bufTam;
	char* paq = creaBuffer(datos.length(), 1, &bufTam, true);
	memcpy(paq + 2 + (datos.length() > pow(2, 16) ? 8 : (datos.length() > 125 ? 2 : 0)), datos.c_str(), datos.length());
// 	this->con << "Paquete a enviar: " << this->hex2str((unsigned char*)paq, bufTam, false) << std::endl;
	retval = enviabin((unsigned char*)paq, bufTam);
	con << "Enviados " << retval << " bytes a " << this->idEquipo << ". ";
	if (retval < 0) {
		con << strerror(errno);
		this->sigue = false;
	}
	con << std::endl;
	free(paq);
	return retval;

}

char* WebSockets::creaBuffer(const unsigned long largoDatos, const unsigned tipo, unsigned *tam, bool final) {
	char* retval;
	unsigned int bufTam = 2, tamTotal = 0;
	bufTam += (largoDatos > pow(2, 16) ? 8 : (largoDatos > 125 ? 2 : 0));
	tamTotal = largoDatos + bufTam;
	char *buf = (char *)malloc(tamTotal);
	//char buf[tamTotal] {0};
	memset(buf, 0x00, tamTotal);
	bitset<8> hdr1;
	bitset<8> hdr2;
	hdr1.set(7, final);
	for (unsigned i = 6; i > 3; i--)
		hdr1.set(i, 0);
	hdr1 |= tipo;
	hdr2.set(7, 0);
	if (bufTam == 2) {
		uint8_t ttam = (uint8_t)largoDatos;
		hdr2 |= ttam;
	} else if (bufTam == 4) {
		hdr2 |= 126;
		memcpy(buf + 2, &largoDatos, 2);
		char tmp = buf[2];
		buf[2] = buf[3];
		buf[3] = tmp;
	} else {
		hdr2 |= 127;
		memcpy(buf + 2, &largoDatos, 8);
		//Invertir el orden según adecuado...
		flipEndian(buf, 2, 8);
	}
	buf[0] = (uint8_t)hdr1.to_ulong();
	buf[1] = (uint8_t)hdr2.to_ulong();
	retval = buf;
	*tam = tamTotal;
	return retval;
}

void WebSockets::flipEndian(char *buf, const unsigned posInicial, const unsigned numBytes) {
	char tmp = 0x00;
	for (unsigned i = 0; i < ((numBytes - posInicial) / 2); i++) {
		unsigned posDest = (posInicial + (numBytes - 1)) - i;
		tmp = buf[posDest];
		buf[posDest] = buf[i + posInicial];
		buf[i + posInicial] = tmp;
	}
}

bool WebSockets::notifica() {
// 	this->milog->println("Vamos a notificar a " + this->idEquipo, 1);
	return (this->enviastr("A") > 1);
// 	this->milog->println("Listo!", 1);
}
/*
bool WebSockets::posicionNueva(const string posicion) {
	return (this->enviastr("C|" + posicion) > 1);
}
*/
bool WebSockets::pingea(string msg) {
	this->tmrPing = 0;
	int retval;
	unsigned bufTam;
	char* paq = creaBuffer(msg.length(), 9, &bufTam, true);
	this->con << "Pingeando con " << msg << std::endl;
	strcat(paq, msg.c_str());
	this->con << "Paquete a enviar: " << hex2str((unsigned char*)paq, bufTam, false) << std::endl;
	retval = (enviabin((unsigned char*)paq, bufTam) > 0);
	con << "Enviados " << retval << " bytes\n";
	return retval;
}

bool WebSockets::enviaComando(Mensaje * cmd) {
	return (this->enviastr(cmd->comando) > 0);
}
WebSockets::~WebSockets() {
	this->qry.str("");
	this->qry << "delete from websocket where cod_usuario = '" << this->idEquipo << "';";
	this->grabaDB(this->qry.str());
	this->qry.str("");
}

/*
bool WebSockets::respondeComando(const string respuesta) {
	return (this->enviastr("D|" + respuesta) > 0);
}
*/
