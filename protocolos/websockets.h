#ifndef __WEBSOCKETS
#define __WEBSOCKETS
#include <string.h>
#include <unistd.h>
#include <iomanip>
#include <math.h>
#include <bitset>
#include <openssl/sha.h>

#include "../superclases/protocolo.h"
#include "../DBConexion.h"
#include "../../iolib/IOCoreFuncs.h"
#define LANZA_PING 70

class WebSockets : public Protocolo {
public:
	WebSockets(const unsigned int f, DBConexion * dbcon, IOCore::IOLogger * l, string p);
	~WebSockets();
	bool activo() override;
	int enviastr(string datos) override;
	bool enviaComando(Mensaje* cmd) override;
	bool notifica();
	/*
	bool posicionNueva(const string posicion);
	bool respondeComando(const string respuesta);
	*/
	
private:
	int				err;
	string			miIP;
	bool			platicando, esFin, hayFrag, sigue;
	long			tamFrag;
	uint8_t			tmrPing;
	stringstream	qry;
	//Este protocolo tiene una lista de equipos...

	//funciones:
	char* creaBuffer(const unsigned long largoDatos, const unsigned tipo, unsigned *tam, bool final);
	void flipEndian(char *buf, const unsigned posInicial, const unsigned numBytes);
	bool pingea(string msg);
};
#endif

