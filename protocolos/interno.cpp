#include "interno.h"

Interno::Interno(const unsigned int f, DBConexion* dbcon, IOCore::IOLogger* l, string p) : Protocolo(f, p, dbcon) {
	this->milog = l;
	this->err = 0;
}

bool Interno::activo() {
	bool retval = true, leido = true, responde = true;
	unsigned char terminador[4];
	uint8_t msgLen = 0;
// 	uint64_t estado;
	unsigned i = 0, pos = 0, gsm, gps;
	uint16_t velocidad, curso;
	string strpaso = "";
	vector< string >::iterator ix;	//Iterador para buscar en un vector
	union {
		unsigned char ch[4];
		int ent;
	} lat, lng;
	union {
		uint64_t mival;
		uint8_t temp_array[8];
	} u;
	double latitud, longitud;
	stringstream msg;
	memset(this->buff, 0x0, MAX_MENSAJE);
	this->err = this->leeSocket(this->buff, 5);	//Leemos el encabezado completo, 19 chars...
	con.str("");
	//Necesitamos una marca de inicio...
	//0x00 0x00 0x52 0x4E
	if (this->err == 5) {
		con << "=== activo(): ";
		if (this->buff[0] == 0x00 && this->buff[1] == 0x00 && this->buff[2] == 0x52 && this->buff[3] == 0x4E) {
			//Vamos a leer el resto del mensaje.
			msgLen = this->buff[4];
			if ((this->err = this->leeSocket(this->buff, msgLen)) == msgLen) {
				//Vamos a leer aquí el terminador, y si es válido, procedemos a la interpretación
				if (this->leeSocket(terminador, 4) == 4) {
					if (terminador[0] == 0xFF && terminador[1] == 0xFF && terminador[2] == 0x52 && terminador[3] == 0x4E) {
						switch (this->buff[0]) {
							case 0x00:	//Saludo: longitud del ID, Identificador del protocolo, más datos...
								this->idEquipo = this->extraeStr(2, msgLen - 2);
								this->largoID = this->buff[1];
								con << "Protocolo nuevo: " << this->idEquipo << " y su ID mide " << (int)this->largoID << std::endl;
								break;
							case 0x01: //0x01 - Nuevo Equipo. ID equipo, {Len ID} bytes
								strpaso = extraeStr(1, this->largoID);
// 								con << "Paquete de nuevo equipo: " << hex2str(this->buff, this->err, false) << std::endl;
								this->listaEquipos.push_back(strpaso);
								//Sí, vamos a enviar notificación de que llegó
								this->concentradora->envia("F|" + strpaso, this->idEquipo, strpaso, DEST_INTERNO, USR_WEBSOCKET);
								break;
							case 0x02:	//0x02 - Actualiza datos. (id, lat, lng, situación (velocidad, curso, señal y GPS), estado)
								pos = 1;
								strpaso = extraeStr(pos, this->largoID);
								//Si el equipo no está en la lista de vivos (pero recibió una actualización, ergo está vivo, lo añadimos)
								ix = find(this->listaEquipos.begin(), this->listaEquipos.end(), strpaso);
								if (ix == this->listaEquipos.end()) this->listaEquipos.push_back(strpaso);
								msg << "C|" << strpaso << "|";
								pos += this->largoID;
								msg << setprecision(15);
								for (i = 0; i < 4; i++) lat.ch[i] = buff[pos++];
								for (i = 0; i < 4; i++) lng.ch[i] = buff[pos++];
								latitud = (double)lat.ent / 10000000.0;
								longitud = (double)lng.ent / 10000000.0;
								msg << latitud << "," << longitud << "|";
								velocidad = BYTES_INT16(pos);
								pos += 2;
								curso = BYTES_INT16(pos);
								pos += 2;
								msg << velocidad << "," << curso << ",";
								gsm = (this->buff[pos] & 0b00001111);
								gps = (this->buff[pos] & 0b11110000) >> 4;
								msg << gps << "," << gsm << "|";
								pos += 1;
								for (i = 0; i < 8; i++) u.temp_array[i] = buff[pos++];
								msg << u.mival;
								con << "Recibió una actualización de posición y estado, mensaje: " << msg.str() << std::endl;
								this->concentradora->envia(msg.str(), this->idEquipo, strpaso, DEST_INTERNO, USR_WEBSOCKET);
								break;
							case 0x03:
								//enviar el mensaje de alerta... Parámetro: el equipo que la levantó...
								strpaso = extraeStr(1, this->largoID);
								msg << "A|" << strpaso;
								this->concentradora->envia(msg.str(), this->idEquipo, strpaso, DEST_INTERNO, USR_WEBSOCKET);
								con << "Recibe una alerta de equipo!\n";
								break;
							case 0x04:	//Se desconecta un equipo...
								strpaso = extraeStr(1, this->largoID);
								con << "Se va el equipo " << strpaso << std::endl;
								ix = find(this->listaEquipos.begin(), this->listaEquipos.end(), strpaso);
								if (ix != this->listaEquipos.end()) {
									this->listaEquipos.erase(ix);
									this->concentradora->envia("G|" + strpaso, this->idEquipo, strpaso, DEST_INTERNO, USR_WEBSOCKET);
								}
								break;
							case 0x05:	//Respuesta en vivo de un comando
								strpaso = extraeStr(1, this->largoID);
								con << "Responde comando el equipo: " << strpaso;
								msg << "D|" << strpaso << "|" << extraeStr(1 + this->largoID, msgLen - (this->largoID + 1));
								this->concentradora->envia(msg.str(), this->idEquipo, strpaso, DEST_INTERNO, USR_WEBSOCKET);
							default:
								con << "Caso no detectado, revisar en crudos...\n";
								leido = false;
								break;
						}
					} else {
						con << "Paquete inválido, terminador incorrecto!!\n";
					}
				} else {
					con << "Paquete incompleto, falta terminador!!\n";
				}
			}
		} else {
			con << "NO RECONOCIDO: " << hex2str(this->buff, this->err, false) << ", leidos: " << this->err << std::endl;
			leido = false;
			retval = false;
		}
	} else if (this->err < 0) {
		if (this->pausas) sleep(1);
	} else {
		retval = false;
	}
	if (this->pausas) usleep(250 * 1000);
	if (!con.str().empty()) {
		this->milog->println(con.str(), 4);
		this->mandaCrudo(leido);
	}
	return retval;
}

std::__cxx11::string Interno::extraeStr(const unsigned int pos, const unsigned largo) {
	stringstream con;
	char paso[largo + 1];
	memset(paso, 0x00, largo + 1);
	memcpy(paso, (char*)this->buff + pos, largo);
	return string(paso);
}

bool Interno::enviaComando(Protocolo::Mensaje* cmd) {
	unsigned tamMsg = cmd->comando.length() + cmd->idEquipo.length() + 1;
	unsigned char buff[tamMsg + 10];
	unsigned pos = 0;
	buff[pos++] = 0xFF;	//0xFF
	buff[pos++] = 0xFF;	//0xFF
	buff[pos++] = 0x52;	//R
	buff[pos++] = 0x4E;	//N
	buff[pos++] = tamMsg;
	buff[pos++] = 0xF0;
	memcpy(buff + pos, cmd->idEquipo.c_str(), cmd->idEquipo.length()); pos += cmd->idEquipo.length();
	memcpy(buff + pos, cmd->comando.c_str(), cmd->comando.length()); pos += cmd->comando.length();
	buff[pos++] = 0x00;	//0x00
	buff[pos++] = 0x00;	//0x00
	buff[pos++] = 0x52;	//R
	buff[pos++] = 0x4E;	//N
	return (enviabin(buff, pos) > 0);
}

	//Ya saludó?
			//0xFF - Primer byte longitud del ID.
			// -- el resto es el ID del equipo, o nombre del protocolo...
			
			
	//Ya cotorrea...
	//Mandamos tamaño de mensaje?
	// 0xFF --Tamaño máximo...
	//Carga...
	//	Tipo de mensaje
		//0x01 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 
		
		/*0x02 
			0xFF 0xFF 0xFF 0xFF latitud  / 10,000,000
			0xFF 0xFF 0xFF 0xFF longitud / 10,000,000
			0xFF 0xFF	velocidad
			0x0F (primeros 4 bits GSM) 0xF0 (segundos cuatro bits GPS)
			0xFF 0xFF curso.
			(8 bytes) estado...
		*/
		//0x03 - Alarma. (Forza la actualización del front...)
		//0x04 - Sale equipo. ID equipo
		//0x01 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 
	//Terminador?
		//0xFF 0xFF 0x52 0x4E
	
