#ifndef __INTERNO__
#define __INTERNO__
#include <string.h>
#include <unistd.h>
#include <iomanip>

#include "../superclases/protocolo.h"
#include "../DBConexion.h"
#include "../../iolib/IOCoreFuncs.h"

/**
 * Protocolo interno de comunicaciones.
 * 
 * Escucha las conexiones de clientes remotos y los almacena aquí.
 * 
 * Instancias de esta clase solamente tienen sentido en el servidor de websockets...
 * 
 */

class Interno : public Protocolo {
public:
	Interno(const unsigned int f, DBConexion * dbcon, IOCore::IOLogger * l, string p);
	bool activo() override;
	bool enviaComando(Mensaje* cmd) override;
	
private:
	int err;
	uint8_t largoID;
	unsigned char buff[MAX_MENSAJE];
	
	string extraeStr(const unsigned pos, const unsigned largo);
};
#endif
