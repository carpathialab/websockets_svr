/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Horus S. Rico <horus.sandoval@carpathialab.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mensaje.h"

Etiqueta::Etiqueta(string definicion) {
	//tomamos los datos del string
	vector< string > datos = IOCore::split(definicion, ",");
	if (datos.size() == 3) {
		this->tag = datos.at(0);
		this->pos = atoi(datos.at(1).c_str());
		this->largo = atoi(datos.at(2).c_str());
	}
}

Mensaje::Mensaje(string definicion) {
	vector< string > lineas = IOCore::split(definicion, "|");
	for (unsigned i = 0; i < lineas.size(); i++) {
		Etiqueta * e = new Etiqueta(lineas[i]);
		if (e->tag != "") this->etiquetas.push_back(e);
		else delete e;
	}
}
