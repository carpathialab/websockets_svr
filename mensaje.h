/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Horus S. Rico <horus.sandoval@carpathialab.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ETIQUETA_H
#define ETIQUETA_H
#include <string>
#include <vector>
#include "../iolib/IOCoreFuncs.h"

using namespace std;
class Etiqueta {
public:
	Etiqueta(string definicion);
	string	tag;
	int		pos;
	int		largo;
};

class Mensaje {
public:
	Mensaje(string definicion);
	
private:
	string					nombre;
	vector< Etiqueta * >	etiquetas;
};

#endif // ETIQUETA_H
