#ifndef _ESTADOMAN_
#define _ESTADOMAN_
#include <vector>
#include <string>
#include <sstream>

#include "DBConexion.h"

using namespace std;

class EstadoMan {
public:
	struct Estado {
		char * disparador;
		uint64_t id;
		string nombre;
		unsigned lenDisparador;
		Estado(std::istream* obj, const uint64_t id, const std::__cxx11::string nombre, const unsigned int tamDisp);
		~Estado();
	};
	EstadoMan(const string protocolo, DBConexion * db);
	~EstadoMan();
	Estado * busca(const char * e, const unsigned tam);
	Estado * busca(const char e);
	uint64_t acumula(const char e);
	uint64_t acumula(const char * e, const unsigned tam);
private:
	vector < Estado* >	estados;
	DBConexion *		dbcon;

	bool compara(const char* e, const char* d, const unsigned int tam);
};

#endif
