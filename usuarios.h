#ifndef __USUARIOS
#define __USUARIOS
#include<iostream>
#include<vector>
#include <string>
#include <sstream>
#include <iomanip>
#include "../iolib/IOCoreFuncs.h"
#include "../iolib/IOLogger.h"
#include "./superclases/protocolo.h"

class Usuarios {
public:
	void agregaUsuario(Protocolo* elnuevo);
	void borraUsuario(const unsigned fd);
	void setLogger(IOCore::IOLogger* elLogger);

	unsigned numClientes();
	int existeUsuario(const string id_dev);

	Protocolo* getCliente(const unsigned id);
	Protocolo* getCliente(const string idEquipo);
	Protocolo* at(const unsigned pos);

	string getUsuariosHR();

private:
	vector< Protocolo * >	lista;
	IOCore::IOLogger *		milog;
};
#endif
