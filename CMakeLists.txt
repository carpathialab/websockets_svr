cmake_minimum_required(VERSION 2.4)
project(websockets_svr)
find_package( Threads )

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11 -g -O0")
set(CMAKE_BUILD_TYPE Debug)

add_executable(websockets_svr usuarios.cpp iosvrmain.cpp ../iolib/ioconfigurador.cpp ../iolib/IOCoreFuncs.cpp ../iolib/IOLogger.cpp DBConexion.cpp superclases/protocolo.cpp superclases/crc.cpp protocolos/websockets.cpp estadoman.cpp superclases/servidor.cpp interconexion/mitotiani.cpp protocolos/interno.cpp superclases/utilerias.cpp)

#set_target_properties(eco.ix PROPERTIES COMPILE_DEFINITIONS "_ECOSVR_")
set_target_properties(websockets_svr PROPERTIES COMPILE_DEFINITIONS "_WEBSOCKETSSVR_")

target_link_libraries(websockets_svr ${CMAKE_THREAD_LIBS_INIT} mysqlcppconn ssl crypto)

install(TARGETS websockets_svr RUNTIME DESTINATION bin)
