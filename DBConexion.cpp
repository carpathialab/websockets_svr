#include "DBConexion.h"

class CharBuf : public streambuf {
public:
   CharBuf(char * d, size_t s) {
      setg(d, d, d + s);
   }
};

DBConexion::DBConexion(const string svr, const string usr, const string pwd, const string dbnom) {
	this->servidor = svr;
	this->usuario = usr;
	this->dbpwd = pwd;
	this->dbnombre = dbnom;
	conectaDB();
}

string DBConexion::getUsuario() {
	return this->usuario;
}
string DBConexion::getServidor() {
	return this->servidor;
}
string DBConexion::getDBNombre() {
	return this->dbnombre;
}
string DBConexion::getPwd() {
	return this->dbpwd;
}
void DBConexion::termina() {
	this->dbcon->close();
}
DBConexion::~DBConexion() {
	delete this->dbcon;
}

int DBConexion::ejecutaSQL(string qry, unsigned reintentos) {
	int retval;
	//Meter depuración avanzada para websockets...
	if (reintentos > 0) usleep(100 * 1000);
	if (reintentos++ < MAX_REINTENTOS) {
		if (this->conectado || this->conectaDB()) {
			try {
				stmt = dbcon->createStatement();
				retval = stmt->executeUpdate(qry);
				stmt->close();
				delete stmt;
			} catch (SQLException sex) {
				if (sex.getErrorCode() == 2013 || sex.getErrorCode() == 2006) {
					this->conectado = false;
					retval = this->ejecutaSQL(qry, reintentos);
				} else {
					retval = -1;
					this->ultError = sex.what();
				}
			}
		} else {
			this->conectado = false;
			retval = this->ejecutaSQL(qry, reintentos);
		}
	} else {
		retval = -1;
	}
	return retval;
}

int DBConexion::ejecutaBinario(string qry, const unsigned char * b, const size_t pos) {
	int retval = -1;
	if (this->conectado || this->conectaDB()) {
		try {
			CharBuf c((char*)b, pos);
			istream s(&c);
			sql::PreparedStatement * ejecutor = this->dbcon->prepareStatement(qry);
			ejecutor->setBlob(1, &s);
			retval = ejecutor->executeUpdate();
			ejecutor->close();
			delete ejecutor;
		} catch (sql::SQLException sex) {
			cerr << "Error SQL: " << sex.getErrorCode() << " -- " << sex.what();
			this->ultError = sex.what();
			this->conectado = false;
		}
	}
	return retval;
}

bool DBConexion::conectaDB() {
	bool		retval;
	sql::Driver	* ctrl;
	
	try {
		ctrl = get_driver_instance();
		dbcon = ctrl->connect(this->servidor, this->usuario, this->dbpwd);
		dbcon->setSchema(this->dbnombre);
		retval = true;
	} catch (sql::SQLException &err) {
		cerr<<"Problemas con las base de datos "<<err.what()<<"\n";
		this->ultError = err.what();
		retval = false;
	}
	this->conectado = retval;
	return retval;
}

ResultSet * DBConexion::select(string qry, unsigned int reintentos) {
	ResultSet * retVal = 0L;
	Statement * stmt = 0L;
	if (reintentos++ < MAX_REINTENTOS) {
		if (this->conectado || this->conectaDB()) {
			try {
				if (this->dbcon != nullptr) {
					stmt = this->dbcon->createStatement();
					stmt->setQueryTimeout(120);
					retVal = stmt->executeQuery(qry);
					stmt->close();
					delete stmt;
				} else {
					retVal = nullptr;
				}
			} catch (SQLException &sqlex) {
				if (sqlex.getErrorCode() == 2013 || sqlex.getErrorCode() == 2006) {
					this->conectado = false;
					retVal = this->select(qry, reintentos);
				} else {
					this->ultError = sqlex.what();
					retVal = 0L;
				}
			}
		} else {
			this->conectado = false;
			retVal = this->select(qry, reintentos);
		}
	}
	return retVal;
}
std::__cxx11::string DBConexion::ultimoError() {
	string retval = this->ultError;
	this->ultError = "";	
	return retval;
}

std::__cxx11::string DBConexion::quitaFeos(const std::__cxx11::string consulta) {
	stringstream retval;
	for (unsigned i = 0; i < consulta.length(); i++) {
		if (consulta.at(i) == '\'') retval << "\\'";
		else retval << consulta.at(i);
	}
	return retval.str();
}
