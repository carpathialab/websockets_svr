#include "protocolo.h"
Protocolo::Protocolo(int miFD, string prot, DBConexion * db) {
	this->posCrudo = 0;
	this->fd = miFD;
	this->idEquipo = "";
	this->protocolo = prot;
	//Clonamos la conexión a la base de datos, al parecer da broncas con el multithreading
	this->dbCon = new DBConexion(db->getServidor(), db->getUsuario(), db->getPwd(), db->getDBNombre());
// 	this->dbCon->setLogger(this->milog);
	this->regInit = 0;
	this->regFin = 0;
	this->afectaDB = true;
	this->idPendiente = 0;
	this->pausas = true;
	this->rayo = 0;
	this->tipo = 1;		//Como predeterminado pone el tipo "Protocolo"
	this->cmdPendientes = new vector< Mensaje *>();
	offsetTiempo = {0};
	offsetTiempo.tm_hour = 0;   offsetTiempo.tm_min = 0; offsetTiempo.tm_sec = 0;
	offsetTiempo.tm_year = 120; offsetTiempo.tm_mon = 11; offsetTiempo.tm_mday = 1;
}

void Protocolo::setTipo(const uint8_t t) {
	this->tipo = t;
}

Protocolo::~Protocolo() {
	this->dbCon->termina();
	delete this->dbCon;
	delete this->cmdPendientes;
	if (this->regInit > 0) delete this->rsDebug;
	this->milog->println("Termina la instancia del protocolo", 4);
}

int Protocolo::enviabin(unsigned char* datos, unsigned int bytes) {
	if (this->regInit == 0 && this->regFin == 0) {
		return send(this->fd, datos, bytes, 0);
	} else {
		milog->println(hex2str(datos, bytes, false), 1);
		return bytes;
	}
}

void Protocolo::uint162Bytes(uint16_t val, uint8_t* bytes_array) {
	// Crear función que une la memoria
	union {
		uint16_t mival;
		uint8_t temp_array[2];
	} u;
	// Overite bytes of union with float variable
	u.mival = val;
	// Assign bytes to input array
	memcpy(bytes_array, u.temp_array, 2);
	uint8_t tmp = bytes_array[0];
	bytes_array[0] = bytes_array[1];
	bytes_array[1] = tmp;
}

void Protocolo::uint322Bytes(uint32_t val, uint8_t* bytes_array, bool invierte) {
	// Crear función que une la memoria
	union {
		uint32_t mival;
		uint8_t temp_array[4];
	} u;
	u.mival = val;
	// Assign bytes to input array
	memcpy(bytes_array, u.temp_array, 4);
	if (invierte) {
		uint8_t tmp = bytes_array[0];
		bytes_array[0] = bytes_array[3];
		bytes_array[3] = tmp;
		tmp = bytes_array[1];
		bytes_array[1] = bytes_array[2];
		bytes_array[2] = tmp;
	}
}

void Protocolo::setConcentradora(Mitotiani* c) {
	this->concentradora = c;
}

unsigned int Protocolo::bytes2uint(const unsigned char* b, const int posInicial) {
	unsigned int retval = 0;
	retval = b[posInicial] << 24 | b[posInicial + 1] << 16 | b[posInicial + 2] << 8 | b[posInicial + 3];
	return retval;
}

uint64_t Protocolo::bytes2ulong(const unsigned char* b, const int posInicial) {
	union {
		uint64_t mival;
		uint8_t temp_array[8];
	} u;
	for (unsigned i = 0; i < 8; i++) {
		u.temp_array[8 - (i + 1)] = b[posInicial + i];
	}
	return u.mival;
}

int Protocolo::bytes2int(const unsigned char* b, const int posInicial) {
	int retval = 0;
	retval = b[posInicial] << 24 | b[posInicial + 1] << 16 | b[posInicial + 2] << 8 | b[posInicial + 3];
	return retval;
}

long Protocolo::bytes2long(const unsigned char* b, const int posInicial) {
	unsigned long retval = 0;
	for (unsigned int i = 0; i < 8; i++) {
		retval |= b[posInicial + i] << (8 * (8 - i));
	}
	//retval = b[posInicial] << 24 | b[posInicial + 1] << 16 | b[posInicial + 2] << 8 | b[posInicial + 3];
	return retval;
}

int Protocolo::getFD() {
	return this->fd;
}

int Protocolo::leeSocket(unsigned char* dest, const size_t bytes) {
	if (this->regInit == 0 && this->regFin == 0) {
		return leeRed(dest, bytes);
	} else {
		return leeDB(dest, bytes);
	}
}

int Protocolo::leeRed(unsigned char* dest, const size_t bytes) {
	int retval = -2;
	int leidos = 0, reintentos = 0;
	do {
		if (retval != -2) {
			usleep(330 * 1000);
		}
		errno = 0;
		retval = read(this->fd, dest + leidos, (bytes - leidos));
		if (retval > 0) {
			//Guardar en el buffer crudo...
			if (leidos + this->posCrudo < MAX_MENSAJE) memcpy(this->crudo + this->posCrudo, dest + leidos, retval);
			this->posCrudo += retval;
			leidos += retval;
		} else if (errno != EAGAIN) {
			this->con << "Retval: " << retval << ", error: " << strerror(errno) << std::endl;
			return retval;
		}
		if ((++reintentos > MAX_REINTENTOS_LECTURA && (retval == -1 || errno == EAGAIN))) {
			leidos = -99;	//Posíblemente quiera decir que no hay datos...
			break;
		}
	} while (bytes != MAX_MENSAJE && (bytes - leidos) > 0);
	return leidos;
}

int Protocolo::leeDB(unsigned char* dest, const size_t bytes) {
	int retval = -2;
	bool sigue = false;
	if (this->posDebug == MAX_MENSAJE && this->tamDebug == 0) {
		if ((sigue = this->rsDebug->next())) {
			this->milog->println("Procesando ID crudo: " + this->rsDebug->getString(3), 1);
			istream *tmp = this->rsDebug->getBlob(1);
			this->tamDebug = this->rsDebug->getUInt(2);
			tmp->read((char*)this->buffDebug, this->tamDebug);
			posDebug = 0;
			delete tmp;
		} else {
			retval = 0;
		}
	} else {
		sigue = true;
	}
	if (sigue) {
		memcpy(dest, this->buffDebug + posDebug, (bytes == MAX_MENSAJE ? this->tamDebug : bytes));
		posDebug += bytes;
		retval = (bytes == MAX_MENSAJE ? this->tamDebug : bytes);
		if (this->posDebug >= this->tamDebug) {
			this->posDebug = MAX_MENSAJE;
			this->tamDebug = 0;
		}
	} else {
		retval = 0;
	}
	return retval;
}


void Protocolo::mandaCrudo(bool interpretado) {
	if (posCrudo > 0) {
		stringstream qry;
		qry << "insert into crudo (protocolo, rayo, idequipo, paquete, leido) values ('" << this->protocolo << "', '" << std::dec << rayo << "', '" << this->idEquipo << "', ?, '" << interpretado << "');";
		this->dbCon->ejecutaBinario(qry.str(), this->crudo, this->posCrudo);
		posCrudo = 0;
		memset(&this->crudo, 0x0, MAX_MENSAJE);
	}
}

string Protocolo::getIDEquipo() {
	return this->idEquipo;
}

int Protocolo::enviastr(string datos) {
	if (this->regInit == 0 && this->regFin == 0) {
		return send(this->fd, datos.c_str(), strlen(datos.c_str()),0);
	} else {
		this->milog->println(datos, 1);
		return datos.length();
	}
}

// void Protocolo::avisaws() {
// 	this->avisaotro("websockets.ix");
// }

// void Protocolo::avisaotro(const string otro) {
// 	if (this->regInit == 0 && this->regFin == 0) {
// 		this->con << "Enviando batiseñal a " << otro << std::endl;
// 		pid_t pid = atoi(IOCore::ejecuta("pgrep " + otro).c_str());
// 		kill(pid, SIGUSR1);
// 	}
// }


void Protocolo::setDepuracion(const ulong regInit, const ulong regFin, const char filLeido) {
	this->regInit = regInit;
	this->regFin = regFin;
	this->posDebug = MAX_MENSAJE;
	this->tamDebug = 0;
	this->afectaDB = false;
	this->pausas = false;
	//Aprovechando, cargamos el resultset de depuración
	stringstream qry;
	qry << "select paquete, length(paquete), id from crudo where id between " << regInit << " and " << regFin << " and protocolo = '"<<this->protocolo<<"'";
	if (this->IMEI != "") qry << " and idequipo = '" << this->IMEI << "'";
	if (filLeido != 't') qry << " and leido = '" << filLeido << "'";
	qry << " order by rayo, id;";
	this->rsDebug = this->dbCon->select(qry.str());
	milog->println("Consulta a depurar: " + qry.str(), 4);
}

int Protocolo::grabaDB(string qry) {
	int retval = -1;
	if (this->afectaDB) {
		retval = this->dbCon->ejecutaSQL(qry);
		if (retval < 0) {
			this->milog->println("Error ejecutando: " + qry, 2);
			this->milog->println(this->dbCon->ultimoError(), 2);
		}
	} else {
		std::cout << qry << std::endl;
		retval = 1;
	}
	return retval;
}

void Protocolo::setAplicaDB(bool valor) {
	this->afectaDB = valor;
}

void Protocolo::setPausas(bool valor) {
	this->pausas = valor;
}

sql::ResultSet * Protocolo::leeDB(const string qry) {
	return this->dbCon->select(qry);
}

bool Protocolo::getDepuracion() {
	return !(this->regInit == 0 && this->regFin == 0);
}

void Protocolo::setEstadoMan(EstadoMan* e) {
	this->estados = e;
}

void Protocolo::setIMEI(const std::__cxx11::string elIMEI) {
	this->IMEI = elIMEI;
}

Protocolo::TorreCelular::TorreCelular(const unsigned int r, const unsigned int c, const float rx) {
	this->idred = r;
	this->idcelula = c;
	this->nivel_rx = rx;
}

std::__cxx11::string Protocolo::volcado() {
	if (!this->con.str().empty()) {
		this->con << "*** FIN DEL VOLCADO DE REGISTRO DE ACTIVIDAD ***";
		return this->con.str();
	}
	return "";
}

void Protocolo::setRayo(uint32_t r) {
	this->rayo = r;
}

bool Protocolo::hayPendientes() {
	return (!this->con.str().empty());
}

void Protocolo::setSvrRespaldo(std::__cxx11::string svr) {
	this->svrRespaldo = svr;
}

void Protocolo::inicia() {
#ifndef _WEBSOCKETSSVR_
	if (this->regInit == 0) this->intercomm();
#endif
	if (this->regInit == 0) {
		this->hilo = new thread(Protocolo::thProcesa, this);
		this->hilo->detach();
	} else {
		Protocolo::thProcesa(this);
	}
}

void Protocolo::thProcesa(Protocolo* instancia) {
	Mensaje * act = 0L;
	unsigned vueltas = 0;
	stringstream qry, con;
	if (instancia->regInit == 0) {
		srand(instancia->dameTS());
		instancia->setRayo((rand() % 7000) * (instancia->dameTS() / 700));
	} else {
		//TODO Aquí iría la preparación para las pruebas...
	}
	while (instancia->activo()) {
		//Ya procesó la entrada, ahora vamos a enviar los comandos.
#ifndef _WEBSOCKETSSVR
		if (instancia->idEquipo != "" && instancia->cmdPendientes->size() > 0) {
			con << "--- thProcesa(): Enviando " << instancia->cmdPendientes->size() << " comandos diferidos a " << instancia->idEquipo << "!\n";
			do {
				act = instancia->cmdPendientes->at(0);
				if (act->idEquipo == instancia->idEquipo) {
					qry.str("");
					instancia->enviaComando(act);
					qry << "update gprs_cola set enviado = now() where id = '" << act->id << "';";
					instancia->dbCon->ejecutaSQL(qry.str());
				} else {
					instancia->cmdPendientes->erase(instancia->cmdPendientes->begin() + 0);
				}
				delete act;
			} while (instancia->cmdPendientes->size() > 0);
			instancia->milog->println(con.str(), 4);
		}
		if (++vueltas > CICLOS_ENVIA) {
			instancia->intercomm();
			vueltas = 0;
		}
#else
		;
#endif
	}
	instancia->termina();
	delete instancia;
}

bool Protocolo::atiende(const std::__cxx11::string idEq, const uint8_t tipoDest, const uint8_t tipoUsr) {
	if (tipoDest == DEST_EXTERNO && this->tipo == tipoUsr) {
		return (this->idEquipo == idEquipo);
	} else if (tipoDest == DEST_INTERNO) {
		for (unsigned i = 0; i < this->listaEquipos.size(); i++) {
			if (this->listaEquipos.at(i) == idEq && this->tipo == tipoUsr) return true;
		}
	}
	return false;
}

ClienteSocket * Protocolo::getNotificador() {
	return this->concentradora->getNotificador();
}

void Protocolo::termina() {
	//Sacamos el protocolo del mitotero...
	this->concentradora->desfirma(this->fd);
	close(this->fd);
	//Limpiamos la lista de comandos pendientes
	this->cmdPendientes->clear();
	this->listaEquipos.clear();
#ifndef _WEBSOCKETSSVR_
	this->getNotificador()->saleEquipo(this->idEquipo);
#endif
}

double Protocolo::dameTS() {
	time_t t = time(nullptr);
	return difftime(t, mktime(&offsetTiempo));
}

void Protocolo::setSvrNombre(const std::__cxx11::string elNom) {
	this->svrNombre = elNom;
}

void Protocolo::intercomm() {
	milog->println("Revisando mensajes resagados!", 1);
	if (!this->getDepuracion() && this->dbCon != 0L) {
		stringstream qry;
		qry << "select gc.id, gc.imeiequipo, gc.mensaje, gc.remitente from gprs_cola gc inner join equipo e on gc.imeiequipo = e.imei inner join tipo_equipo_protocolo tep on tep.idtipo_equipo = e.idtipo_equipo where tep.idprotocolo = '" << this->svrNombre << "' and gc.enviado is null order by id asc;";
		sql::ResultSet* rs = this->dbCon->select(qry.str());
		if (rs != 0L) {
			//Ya tenemos los registros, vamos a llenar la lista de pendientes.
			while(rs->next()) {
				this->cmdPendientes->push_back(new Protocolo::Mensaje(rs->getUInt(1), rs->getString(2), rs->getString(3), rs->getString(4), this->dameTS()));
			}
			rs->close();
		} else {
			this->milog->println("No hay pendientes de envío...", 4);
		}
		delete rs;
	}
}

void Protocolo::setIPCliente(const std::__cxx11::string ip) {
	this->ipcliente = ip;
}

std::__cxx11::string Protocolo::getIPCliente() {
	return this->ipcliente;
}

uint8_t Protocolo::getTipo() {
	return this->tipo;
}

std::__cxx11::string Protocolo::dimeAtendidos() {
	stringstream ret;
	for (unsigned i = 0; i < this->listaEquipos.size(); i++) {
		ret << this->listaEquipos.at(i) << ", ";
	}
	return ret.str();
}
