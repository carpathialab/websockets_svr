#ifndef _IOSVR_PROTOCOLO
#define _IOSVR_PROTOCOLO
#define MAX_REINTENTOS_LECTURA 20
#define CICLOS_ENVIA 26
#define BYTES_INT16(pos) (buff[pos] << 8 | buff[pos + 1])

class Mitotiani;
class ClienteSocket;
#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <string.h>
#include <iomanip>
#include <sys/types.h>
#include <signal.h>
#include <sys/socket.h>

#include "../DBConexion.h"
#include "../estadoman.h"
#include "../../iolib/IOLogger.h"
#include "../../iolib/IOCoreFuncs.h"
#include "../interconexion/mitotiani.h"
#include "../interconexion/cliente.h"

#include <thread>

#define MAX_MENSAJE 10500

using namespace std;
/** Protocolo
	* @brief Es la clase abstracta para los protocolos
	* @param mensaje puntero a char con los datos del mensaje
	*/
class Protocolo {
public:
	struct Mensaje {
		unsigned long id;
		string idEquipo;
		string remite;
		string comando;
		double momento;
		Mensaje(const unsigned long i, const string eq, const string cmd, const string usr, const double m) {
			this->id = i;
			this->idEquipo = eq;
			this->comando = cmd;
			this->remite = usr;
			this->momento = m;
		}
	};
	struct TorreCelular {
		unsigned idred;
		unsigned idcelula;
		float nivel_rx;
		TorreCelular(const unsigned r, const unsigned c, const float rx);
	};
	struct APWifi {
		char ssid[6];
		string essid;
		float nivel_rx;
		APWifi(const char id[6], const string nom, const float rx);
	};
	/** Constructor
	 * @brief Recibe una fracción de un mensaje de texto que será interpretado en el futuro.
	 * @param mensaje puntero a char con los datos del mensaje
	 */
	Protocolo(int miFD, string prot, DBConexion * db);
	
	/** Destructor
	 * @brief Termina la instancia del protocolo
	 */
	virtual ~Protocolo();
	
	/** activo
	 * @brief Devuelve verdadero si se logró identificar un mensaje válido en ese ciclo, si devuelve falso, la conexión debe terminar y salir del hilo del cliente.
	 * @return true si debe seguir ejecutándose el cliente
	 */
	virtual bool activo() = 0;
	
	/** enviabin
	 * @brief envía una serie de datos binarios, es decir, sin detenerse en los caracteres nulos al FD predeterminado
	 * @param datos apuntador a donde están los datos
	 * @param bytes número de bytes a enviar
	 * @return el numero de bytes enviados
	 */
	virtual int enviabin(unsigned char * datos, unsigned int bytes);
	
	/** enviastr
	 * @brief envía una cadena de texto al FD de la instancia
	 * @param datos cadena de texto que se va a enviar
	 * @return el numero de bytes enviadoss
	 */
	virtual int enviastr(string datos);

	/**
	* hex2str
	* @brief obtiene un string representativo de una cadena hexadecimal
	* @param buff apuntador a unsigned char para tomar la información.
	* @param cantidad cantidad de bytes que va a devolver.
	* @param compacto Si es TRUE devuelve el formato compacto, de otra manera inserta espacios entre bytes
	static string hex2str(const unsigned char* b, const unsigned int l, const bool compacto);
	*/

	/**
	* uint162Bytes
	* @brief Transforma un entero de 16 bits en 2 bytes de información
	* @param uint16_t Entero de 16 bits a transformar
	* @param uint8_t* Apuntador a un arreglo de dos bytes para poner el resultado
	*/
	static void uint162Bytes(uint16_t val,uint8_t* bytes_array);
	/**
	* uint322Bytes
	* @brief Transforma un entero de 32 bits en 4 bytes de información
	* @param uint16_t Entero de 32 bits a transformar
	* @param uint8_t* Apuntador a un arreglo de cuatro bytes para poner el resultado
	* @param bool Verdadero si invierte el orden de los bytes. Como predeterminado: 
	*/
	static void uint322Bytes(uint32_t val, uint8_t* bytes_array, bool invierte = true);
	/**
	* bytes2uint 
	* @brief Transforma cuatro en un entero sin signo. Esta función considera que el orden de los bytes es MSB -> LSB
	* @param char* apuntador a un arreglo de bytes a transformar
	* @param int posición inicial donde debe empezar la transformación (offset)
	* @returns entero sin signo con el valor dado
	*/
	static unsigned int bytes2uint(const unsigned char * b, const int posInicial);
	
	/**
	 * bytes2ulong
	 * @brief Transforma 8 bytes en un largo sin signo. Esta función considera que el orden de los bytes es MSB -> LSB
	 * @param char* apuntador a un arreglo de bytes a transformar
	 * @param int posición inicial donde empezará a leer.
	 * @returns entero largo sin signo con el valor dado.
	 */
	static uint64_t bytes2ulong(const unsigned char* b, const int posInicial);

	/**
	* bytes2int 
	* @brief Transforma cuatro en un entero CON signo. Esta función considera que el orden de los bytes es MSB -> LSB
	* @param char* apuntador a un arreglo de bytes a transformar
	* @param int posición inicial donde debe empezar la transformación (offset)
	* @returns entero sin signo con el valor dado
	*/
	static int bytes2int(const unsigned char * b, const int posInicial);
	
	/**
	 * bytes2long
	 * @brief Transforma 8 bytes en un largo CON signo. Esta función considera que el orden de los bytes es MSB -> LSB
	 * @param char* apuntador a un arreglo de bytes a transformar
	 * @param int posición inicial donde empezará a leer.
	 * @returns entero largo sin signo con el valor dado.
	 */
	static long bytes2long(const unsigned char* b, const int posInicial);
	
	bool getDepuracion();
	
	/** getFD
	 * @brief Obtiene el descriptor de archivo de la instancia
	 * @returns entero del descriptor de archivo
	 */
	virtual int getFD();
	
	/** getIDEquipo
	 * @brief Obtiene el ID del equipo de este protocolo...
	 * @returns cadena de texto con el ID del protocolo
	 */
	virtual string getIDEquipo();
	
	ClienteSocket * getNotificador();
	
	virtual uint8_t getTipo();
	
	virtual string getIPCliente();
	
	virtual string dimeAtendidos();
	/**
	 * setDepuracion
	 * @brief Establece los registros para depuración. Cuando está en depuración desactiva los procesos de red y en lugar de eso utiliza la base de datos como fuente de información.
	 * @param ulong registro inicial donde empezará a leer registro de depuración
	 * @param ulong registro final donde terminará la depuración.
	 */
	void setDepuracion(const ulong regInit, const ulong regFin, char filLeido);
	
	void setAplicaDB(bool valor);
	
	void setPausas(bool valor);
	
	void setIMEI(const string elIMEI);
	
	void setEstadoMan(EstadoMan* e);
	
	void setSvrRespaldo(string svr);
	
	void setTipo(const uint8_t t);
	
	void setConcentradora(Mitotiani * c);
	
	void setSvrNombre(const string elNom);
	
	void setIPCliente(const string ip);
	
	/** enviaComando
	 * @brief Envía un comando al cliente. Todos los protocolos deben de implementar esta función. 
	 * Usualmente el protocolo debe de llevar un control de la respuesta que reciibe a los comandos y no solamente enviarlo. La estructura de los comandos se declara en la clase
	 * Protocolo para que pueda ser accesible a los derivados de esta clase y se pueda llevar el control de las respuestas. Cada implementación de Protocolo deberá llevar un control del mismo.
	 */
	virtual bool enviaComando(Mensaje* cmd) = 0;
	
	/** volcado
	 * @brief Envia a un string toda la información contenida en el buffer de depuración. Esta función está pensada para ser usada en caso de errores y que podamos tener lo pendiente en el log final
	 */
	string volcado();
	
	/** setRayo
	 * @brief Establece el identificador de conexión ("rayo") que tendrá este protocolo.
	 * @param uint32_t ID del "rayo"
	 */
	void setRayo(uint32_t r);
	
	/** hayPendientes
	 * @brief Devuelve verdadero o falso, según si hay pendientes en el buffer de depuración.
	 * @return bool longitud del stringstream mayor que 0
	 */
	bool hayPendientes();
	
	/** atiende
	 * @brief Devuelve verdadero si considera que este protocolo atiende ese ID de equipo
	 * @param string ID del equipo
	 */
	virtual bool atiende(const string idEquipo, const uint8_t tipoDest, const uint8_t tipoUsr);
	
	/** inicia
	 * @brief Arranca el protocolo en forma de hilo
	 */
	void inicia();

	/** termina
	 * @brief Hace la limpieza de salida del protocolo...
	 */
	void termina();
	
protected:
	//Variables...
	IOCore::IOLogger*	milog;
	vector<Mensaje*>*	cmdPendientes;
	EstadoMan *			estados;
	vector < string >	listaEquipos;
	Mitotiani *			concentradora;
	bool				pausas;
	uint8_t				tipo;
	unsigned char		buff[MAX_MENSAJE];
	stringstream		con;
	string				idEquipo;
	string				protocolo;
	string				svrRespaldo;
	string				svrNombre;	//Necesitamos tener el nombre del protocolo al que pertenece...
	string				ipcliente;
	//Solamente se ponen datos aquí si se atienden muchos equipos
	
	//Funciones...
	/** leeSocket
	 * @brief Esta función debe de usarse en lugar de llamar a read. Se comporta básicamente igual, con la diferencia que genera una copia de lo leído en un buffer para el paquete crudo de depuración.
	 * Después de usar esta función, y antes de que la función activo regrese un valor, se debe de llamar a mandaCrudo
	 * @param dest apuntador a void del destino donde pondrá lo leído
	 * @param bytes número de bytes a leer desde el socket.
	 * @returns Entero con la cantidad de bytes leídos. En caso de error regresa -1, 0 indica fin de archivo. Ver la documentación de read para mayores referencias
	 */
	int leeSocket(unsigned char* dest, const size_t bytes);
	
	/** mandaCrudo
	 * @brief Envía el buffer con el paquete crudo a la base de datos.
	 * @param interpretado booleano indicando si el paquete fue interpretado exitosamente o no.
	 */
	void mandaCrudo(bool interpretado);
	
	/** grabaDB
	 * Sirve como intermediario para aislar la conexión de la base de datos de los protocolos derivados. Es necesario ese aislamiento para que establecer las funciones de depuración de protocolos.
	 * @param string Consulta a ejecutar en la DB
	 * @returns int número de filas afectadas
	 */
	int grabaDB(string qry);

	/** leeDB
	 * Sirve como intermediario para aislar la conexión de la base de datos de los protocolos derivados. Es necesario ese aislamiento para que establecer las funciones de depuración de protocolos.
	 * @param string Consulta a ejecutar en la DB
	 * @returns apuntador a un ResultSet con los datos
	 */
	ResultSet * leeDB(const string qry);

	/** @deprecated avisaws
	 * Envía una señal SIGUSR1 al proceso de websockets
	void avisaws();
	
	void avisaotro(const string otro);
	 */
	
private:
	thread *		hilo;
	DBConexion*		dbCon;
	ResultSet *		rsDebug;
	struct tm		offsetTiempo;
	bool			afectaDB;
	unsigned char	crudo[MAX_MENSAJE];
	unsigned char	buffDebug[MAX_MENSAJE];
	unsigned int	posCrudo;
	unsigned		tamDebug;
	ulong			regInit, regFin;
	ulong			idPendiente;
	int				fd;
	int				posDebug;
	uint32_t		rayo;
	string			IMEI;	//Esta variable es para cuando se depura solo un IMEI

	/** thProcesa
	 * @brief La función interna que gestiona la ejecución como hilo independiente de cada protocolo.
	 * @param Protocolo * Apuntador a la instancia del protocolo que se está ejecutando. Típicamente es "this"
	 */
	static void thProcesa(Protocolo * instancia);
	double dameTS();

	/** intercomm
	* @brief Se encarga de gestionar la comunicación entre procesos
	*/
	void intercomm();
	
	int leeRed(unsigned char* dest, const size_t bytes);
	int leeDB(unsigned char* dest, const size_t bytes);

};

#endif
