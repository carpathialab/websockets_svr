#ifndef _UTILERIAS_
#define _UTILERIAS_
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;

struct OpcionesTCP {
	string	errsock;
	string	kaopt;
	string	soka;
	string	soto; 
	string	slin; 
	string	slin2; 
	string	skepid;
};

/** hex2str
 * @brief imprime en formato hexadecimal legible al humano el contenido de un arreglo de bytes.
 * @param uchar* Apuntador al arreglo de bytes a imprimir.
 * @param unsigned El número de bytes que va a imprimir.
 * @param bool Usar formato compacto? El formato compacto no imprime espacio entre bytes.
 * @returns una cadena de texto con el contenido del arreglo.
 */
std::string hex2str(const unsigned char* b, const unsigned int l, const bool compacto);

/** limpia
 * @brief Elimina el apóstrofe de los campos que vamos a usar para transformaciones
 * @param string* Apuntador a la cadena que se va a limpiar
 */
void limpia(string *val);
#endif
