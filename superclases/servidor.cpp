#include "servidor.h"

Servidor::Servidor(const string puerto, const unsigned int maxUsr, Mitotiani * mit, IOCore::IOLogger * log, DBConexion * db, uint8_t tipoU, string nom, Servidor::InfoDebug * dbg) {
	this->TIPO_USUARIOS = tipoU;
	this->milog = log;
	this->puerto = puerto.c_str();
	this->maxUsuarios = maxUsr;
	this->mitote = mit;
	this->fifando = false;
	this->dbcon = db;
	this->nombre = nom;
	this->datosDepura = dbg;
	this->datosDepura->ajusta();
}

void Servidor::thProceso (Servidor * instancia) {
	while(instancia->fifando) {
		int64_t rfd = 0;
		string ipcliente;
		
		rfd = instancia->aceptaConexion(&ipcliente);
		if (rfd >= 0) {
			if (instancia->usuarios.size() + 1 >= instancia->maxUsuarios) {
				instancia->milog->println("El servidor está lleno, abortando la conexión...", 2);
				close(rfd);
			} else {
				Protocolo * p = instancia->generaProtocolo(rfd);
				p->setIPCliente(ipcliente);
				p->setTipo(instancia->TIPO_USUARIOS);
				instancia->mitote->firma(p);
				p->inicia();
			}
		}
	}
}

void Servidor::termina() {
	this->fifando = false;
}

Servidor::~Servidor() {
	this->dbcon->termina();
	delete this->dbcon;
}

void Servidor::inicia() {
	if (!this->estaDepurando()) {
		this->fifando = true;
		//Aquí se pone la parte de la depuración...
		this->hilo = new thread(Servidor::thProceso, this);
// 		this->hilo->detach();
	} else {
		//Mandar valores de depuración a un protocolo nuevo...
		Protocolo * p = this->generaProtocolo(-1);
		p->setIMEI(this->datosDepura->imeiDebug);
		p->setDepuracion(this->datosDepura->regInit, this->datosDepura->regFin, this->datosDepura->leidoEstado);
		p->setAplicaDB(datosDepura->dbAplica);
		p->setPausas(datosDepura->hazPausas);
		p->inicia();
	}
}

bool Servidor::estaDepurando() {
	return !(this->datosDepura->regInit == 0 || this->datosDepura->regFin == 0);
}
/*
void Servidor::ajustaOpciones(const std::__cxx11::string es, const std::__cxx11::string ko, const std::__cxx11::string sk, const std::__cxx11::string st, const std::__cxx11::string sl, const std::__cxx11::string sl2, const std::__cxx11::string skid) {
	this->errsock = es;
	this->kaopt = ko;
	this->soka = sk;
	this->soto = st;
	this->slin = sl;
	this->slin2 = sl2;
	this->skepid = skid;
}
*/
bool Servidor::preparaEscucha(const OpcionesTCP * opts_tcp) {
	stringstream con;
	struct addrinfo hostinfo, *res;
	int err = 0;
	bool retval = false;
	if (!this->estaDepurando()) {
		int opt_int = 1;		//SO_REUSEADDR: Activa la opción SO_REUSEADDR	
		timeval snd_to; snd_to.tv_sec = 10;	//SO_SNDTIMEO: Tiempo de espera en los envíos...
		linger lng; lng.l_onoff = 1; lng.l_linger = 0;	//SO_LINGER: Activa la administración del cierre y cierra la conexión en chinga
		
		memset(&hostinfo, 0, sizeof(hostinfo));
		hostinfo.ai_family = AF_UNSPEC;		//Para que jale con IPv4 o v6 indistintamente.
		hostinfo.ai_socktype = SOCK_STREAM;
		hostinfo.ai_flags = AI_PASSIVE;
		//Supongo que si cambiamos el primer parámetro, podemos atarlo a una dirección específica...
		getaddrinfo(NULL, this->puerto, &hostinfo, &res);
		this->svr_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		if (this->svr_fd >= 0) {
			if (opts_tcp->soka == "1") {
				errno = 0;
				opt_int = 1;
				err += setsockopt(svr_fd, SOL_SOCKET, SO_KEEPALIVE, &opt_int, sizeof(int));
				con << "Encendió el keepAlive: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
				errno = 0;
			}
			if (opts_tcp->soto != "" && opts_tcp->soto != "0") {
				snd_to.tv_sec = atoi(opts_tcp->soto.c_str());
				snd_to.tv_usec = atoi(opts_tcp->soto.c_str()) * 10000;
				err += setsockopt(svr_fd, SOL_SOCKET, SO_SNDTIMEO, &snd_to, sizeof(timeval));
				con << "Estableció el timeout de envio: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
				errno = 0;
			}
			if (opts_tcp->slin == "1") {
				opt_int = 1;
				err += setsockopt(svr_fd, SOL_SOCKET, SO_LINGER, &opt_int, sizeof(linger));
				con << "Pone el linger: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
				errno = 0;
			}
			if (opts_tcp->slin2 != "" && opts_tcp->slin2 != "0") {
				opt_int = atoi(opts_tcp->slin2.c_str());
				err += setsockopt(svr_fd, SOL_SOCKET, TCP_LINGER2, &opt_int, sizeof(int));
				con << "Ajusta el linger2: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
				errno = 0;
			}
			if (opts_tcp->skepid != "" && opts_tcp->skepid != "0") {
				opt_int = atoi(opts_tcp->skepid.c_str());
				err += setsockopt(svr_fd, IPPROTO_TCP, TCP_KEEPIDLE, &opt_int, sizeof(int));
				con << "Ajusta la inactividad de KeepAlive: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
				errno = 0;
			}
			if (opts_tcp->kaopt != "") {
				opt_int = atoi(opts_tcp->kaopt.c_str());
				err += setsockopt(svr_fd, IPPROTO_TCP, TCP_KEEPINTVL, &opt_int, sizeof(int));
				con << "Ajusta el intervalo keepAlive: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
				errno = 0;
			}
			//Esta es la opción que venía originalmente en el ejemplo...
			opt_int = 1;
			err += setsockopt(svr_fd, SOL_SOCKET, SO_REUSEADDR, &opt_int, sizeof(int));
			con << "Después de establecer las opciones del socket: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
			errno = 0;
			if (err >= 0) {
				err = bind(this->svr_fd, res->ai_addr, res->ai_addrlen);
				if (err == 0) {
					err = listen(this->svr_fd, Servidor::EN_ESPERA);
					if (err >= 0) {
						con << "Servidor iniciado exitosamente\n";
						this->fifando = true;
						retval = true;
					} else {
						con << "Error escuchando...\n";
					}
				} else {
					con << "Error amarrando el puerto...\n";
				}
			} else {
				con << "Error estableciendo las opciones del socket\n";
			}
		} else {
			con << "Error creando el socket\n";
		}
		this->milog->println(con.str(), 4);
		return retval;
	} else {
		return true;
	}
}

int Servidor::aceptaConexion(string *ipcliente) {
	stringstream con;
	char ipstr[INET6_ADDRSTRLEN];
	int port;
	int nvoCliente;
	struct sockaddr_storage remote_info ;
	socklen_t addr_size;
	con << "--- aceptaConexion(): ";
	addr_size = sizeof(addr_size);
	nvoCliente = accept(this->svr_fd, (struct sockaddr *) &remote_info, &addr_size);
	fcntl(nvoCliente, F_SETFL, O_NONBLOCK);
	if (nvoCliente >= 0) {
		getpeername(nvoCliente, (struct sockaddr*)&remote_info, &addr_size);
		// deal with both IPv4 and IPv6:
		if (remote_info.ss_family == AF_INET) {
			struct sockaddr_in *s = (struct sockaddr_in *)&remote_info;
			port = ntohs(s->sin_port);
			inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
		} else { // AF_INET6
			struct sockaddr_in6 *s = (struct sockaddr_in6 *)&remote_info;
			port = ntohs(s->sin6_port);
			inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
		}
		*ipcliente = string(ipstr);
		con << "Acepta conexión desde " << ipstr;
	} else {
		con << "Error " << errno << " al aceptar nueva conexión\n";
		nvoCliente = -1;
	}
	this->milog->println(con.str(), 4);
	return nvoCliente;
}

Protocolo * Servidor::generaProtocolo(int elFD) {
	Protocolo * prot = 0L;
	EstadoMan * edoMan = new EstadoMan(this->nombre, this->dbcon);
#ifdef _ECOSVR_
	prot = new Eco(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _QBITSVR_
	prot = new Qbit(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _ST600SVR_
	prot = new ST600(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _ST410SVR_
	prot = new ST410G(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _ST300SVR_
	prot = new ST300(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _ST200SVR_
	prot = new ST200(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _ST4340SVR_
	prot = new ST4340(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _FA24_4GSVR_
	prot = new FA28_4g(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _MANTENIMIENTOSVR_
	prot = new Mantenimiento(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _TELTONIKASVR_
	prot = new Teltonika(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _GT06E_
	prot = new GT06E(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _GT08_
	prot = new GT08(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _SP1600_
	prot = new SP1600(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _QUECLINK_
	prot = new Queclink(elFD, this->dbcon, milog, this->nombre);
#endif
#ifdef _WEBSOCKETSSVR_
	if (this->TIPO_USUARIOS == USR_INTERNO) {
		cout << "Instancia de interno\n";
		prot = new Interno(elFD, this->dbcon, milog, this->nombre);
	} else {
		prot = new WebSockets(elFD, this->dbcon, milog, this->nombre);
	}
#else
	if (edoMan == 0L) edoMan = new EstadoMan(this->nombre, this->dbcon);
	prot->setEstadoMan(edoMan);
// // 	prot->conectaNotif(this->svrInterno, this->ptoInterno);
// 	prot->setSvrRespaldo(miconf->leeValor("svrrespaldo"));
#endif
	prot->setSvrNombre(this->nombre);
	return prot;
}
// void Servidor::ajustaInterno(std::__cxx11::string svr, uint16_t pto) {
// 	this->svrInterno = svr;
// 	this->ptoInterno = pto;
// }

void Servidor::InfoDebug::ajusta() {
	if (this->regFin > 0 && this->regInit == 0)
		this->regInit = 1;	//Si solamente se establece el último registro, se lee desde el registro 1
	else if (this->regInit > 0 && this->regFin == 0)
		this->regFin = this->regInit;	//Si solamente se establece el primero, solamente se lee ese registro
}
