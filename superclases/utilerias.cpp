#include "utilerias.h"

string hex2str(const unsigned char* b, const unsigned int l, const bool compacto) {
	std::stringstream buff;
	for (unsigned i = 0; i < l; i++) {
		buff << std::hex << std::setw(2) << std::setfill('0') << unsigned(b[i]);
		if (!compacto) buff << " ";
	}
	return buff.str();
}

void limpia(string *val) {
	size_t it;
	while ((it = val->find("'")) != string::npos) {
		val->erase(it, 1);
	}
}
