#ifndef __SERVIDOR__
#define __SERVIDOR__ 0
#include <thread>
#include <vector>
#include <string.h>
#include <sstream>
#include "../interconexion/mitotiani.h"
#include "../estadoman.h"

/**
 * Conexiones TCP, encabezados, etc...
 */
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <fcntl.h>

/**
 * Utilerías
 */
#include "../../iolib/IOLogger.h"
#include "utilerias.h"

#include "protocolo.h"
#ifdef _ECOSVR_
#include "../protocolos/eco.h"
#endif
#ifdef _QBITSVR_
#include "../protocolos/qbit.h"
#endif
#ifdef _ST600SVR_
#include "../protocolos/st600.h"
#endif
#ifdef _ST410SVR_
#include "../protocolos/st410g.h"
#endif
#ifdef _ST300SVR_
#include "../protocolos/st300.h"
#endif
#ifdef _ST200SVR_
#include "../protocolos/st200.h"
#endif
#ifdef _ST4340SVR_
#include "../protocolos/st4340.h"
#endif
#ifdef _FA24_4GSVR_
#include "../protocolos/fa28_4g.h"
#endif
#ifdef _MANTENIMIENTOSVR_
#include "../protocolos/mantenimiento.h"
#endif
#ifdef _TELTONIKASVR_
#include "../protocolos/teltonika.h"
#endif
#ifdef _GT06E_
#include "../protocolos/gt06e.h"
#endif
#ifdef _GT08_
#include "../protocolos/gt08.h"
#endif
#ifdef _SP1600_
#include "../protocolos/sp1600.h"
#endif
#ifdef _QUECLINK_
#include "../protocolos/queclink.h"
#endif
#ifdef _WEBSOCKETSSVR_
#include "../protocolos/websockets.h"
#include "../protocolos/interno.h"
#define LATENCIA_MSG 30.0
#else
#define LATENCIA_MSG 7200
#endif

using namespace std;
using namespace IOCore;

class Servidor {
public:
	struct InfoDebug {
		unsigned long	regInit, regFin;
		string			imeiDebug;
		bool			dbAplica, hazPausas;
		char			leidoEstado;
		void ajusta();
	};
	Servidor (const std::__cxx11::string puerto, const unsigned int maxUsr, Mitotiani * mit, IOCore::IOLogger* log, DBConexion * db, uint8_t tipoU, string nom, InfoDebug * dbg);
	bool preparaEscucha(const OpcionesTCP * opts_tcp);
	void inicia();
// 	void ajustaOpciones(const string es, const string ko, const string sk, const string st, const string sl, const string sl2, const string skid);
// 	void ajustaInterno(string svr, uint16_t pto);
	void termina();
	~Servidor();

	thread *			hilo;
	
private:
	//Función que hace la función del hilo
	static void thProceso(Servidor * s);
	//Funciones para instancia
	int aceptaConexion(string *ipcliente);
	Protocolo * generaProtocolo(int elFD);
	bool estaDepurando();
	
	IOLogger *			milog;
	DBConexion *		dbcon;
	Mitotiani *			mitote;
	vector <thread *>	usuarios;
	InfoDebug *			datosDepura;
	const char *		puerto;
	int					svr_fd;
	unsigned int		maxUsuarios;
	uint16_t			ptoInterno;
	uint8_t				TIPO_USUARIOS;
	bool				fifando;
	bool				verboso;
	string				nombre;
	string				svrInterno;
	//Opciones del socket ajustables
// 	string				errsock, 
// 						kaopt, 
// 						soka, 
// 						soto, 
// 						slin, 
// 						slin2, 
// 						skepid;
	//Variables para los Threads...
	static const int	EN_ESPERA = 5;	//Conexiones que pueden estar esperando a conectar
};
#endif
