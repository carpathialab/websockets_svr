#include "usuarios.h"
#include <list>

using namespace std;

void Usuarios::agregaUsuario(Protocolo * elnuevo) {
	this->lista.push_back(elnuevo);
}

int Usuarios::existeUsuario(const string usuario) {
	//Recordemos que hay que regresar -1 si no está
	int retval = -1;
	for (unsigned i = 0; i < lista.size(); i++) {
		if (lista[i]->getIDEquipo() == usuario) {
			retval = i;
			break;
		}
	}
	return retval;
}

Protocolo * Usuarios::getCliente(const unsigned int id) {
	Protocolo *retval = NULL;
	try {
		for (unsigned i = 0; i < lista.size(); i++) {
			if (lista[i]->getFD() == id)
				retval = lista[i];
		}
	} catch (exception ex) {
		char err[100];
		sprintf(err, "No encontró usuario %d \n", id);
		milog->println(string(err), 2); 
	}
	return retval;
}

Protocolo * Usuarios::getCliente(const string idEquipo) {
	Protocolo *retval = NULL;
	for (unsigned i = 0; i < lista.size(); i++) {
		if (lista[i]->getIDEquipo() == idEquipo) {
			retval = lista[i];
			break;
		}
	}
	return retval;
}

unsigned int Usuarios::numClientes() {
	return lista.size();
}

void Usuarios::borraUsuario(const unsigned int id) {
	for (unsigned i = 0; i < lista.size(); i++) {
		if (lista[i]->getFD() == id) {
			lista.erase(lista.begin() + i);
		}
	}
}

string Usuarios::getUsuariosHR() {
	stringstream retval;
	retval << std::string(73, '-') << "\n" << setw(7) << "id |" << setw(7) << "FD |" << setw(17) << " ID dispositivo|\n";
	retval << std::string(73, '-') << "\n";
	unsigned i;
	for (i = 0; i < this->lista.size(); i++) {
		retval << setw(5) << i << " |" << setw(5) << lista[i]->getFD() << " |" << setw(15) << (lista[i]->getIDEquipo() == "" ? "(anonimo)" : lista[i]->getIDEquipo()) << " |\n";
	}
	retval << std::string(73, '-') << "\n*** Fin del listado ***\n";
	return retval.str();
}

Protocolo* Usuarios::at(const unsigned int pos) {
	Protocolo * retval = 0L;
	if (pos < lista.size())
		retval = lista.at(pos);

	return retval;
}

void Usuarios::setLogger(IOCore::IOLogger* elLogger) {
	milog = elLogger;
}
