#include "cliente.h"
#include <netinet/tcp.h>

ClienteSocket::ClienteSocket(const string p, const std::__cxx11::string ip, IOLogger* log, Mitotiani * prop, bool depura, const OpcionesTCP * opts) {
	this->destino = ip;
	this->puerto = p;
	this->sock = 0;
	this->milog = log;
	this->propietario = prop;
	this->modoDebug = depura;
	this->proc = 0L;
	this->toks.push_back(new Token('\'', '\'', 0x01));
	this->toks.push_back(new Token('(', ')', 0x02));
	this->opts_tcp = opts;
	if (!this->modoDebug) {
		if (this->contecta()) {
			this->fifando = true;
			this->inicia();
		}
	}
}

ClienteSocket::Token::Token(const char i, const char f, const uint8_t msk) {
	this->inicio = i;
	this->fin = f;
	this->mascara = msk;
}

bool ClienteSocket::contecta() {
	bool retval = false;
	int err = 0, opt_int = 1;
	stringstream con;
	addrinfo banderas, *p;
	timeval snd_to; snd_to.tv_sec = 10;	//SO_SNDTIMEO: Tiempo de espera en los envíos...
	memset(&banderas, 0, sizeof(banderas));
	banderas.ai_family = AF_UNSPEC;
	banderas.ai_socktype = SOCK_STREAM;
	banderas.ai_flags = AI_PASSIVE;
	con << "=== conecta(): ";
	int laDir = getaddrinfo(this->destino.c_str(), this->puerto.c_str(), &banderas, &p);
	if (laDir == 0) {
		if (p != 0L) {
			this->sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
			if (this->sock != -1) {
				if (opts_tcp->soka == "1") {
					errno = 0;
					opt_int = 1;
					err += setsockopt(this->sock, SOL_SOCKET, SO_KEEPALIVE, &opt_int, sizeof(int));
					con << "Encendió el keepAlive: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
					errno = 0;
				}
				if (opts_tcp->soto != "" && opts_tcp->soto != "0") {
					snd_to.tv_sec = atoi(opts_tcp->soto.c_str());
					snd_to.tv_usec = atoi(opts_tcp->soto.c_str()) * 10000;
					err += setsockopt(this->sock, SOL_SOCKET, SO_SNDTIMEO, &snd_to, sizeof(timeval));
					con << "Estableció el timeout de envio: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
					errno = 0;
				}
				if (opts_tcp->slin == "1") {
					opt_int = 1;
					err += setsockopt(this->sock, SOL_SOCKET, SO_LINGER, &opt_int, sizeof(linger));
					con << "Pone el linger: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
					errno = 0;
				}
				if (opts_tcp->slin2 != "" && opts_tcp->slin2 != "0") {
					opt_int = atoi(opts_tcp->slin2.c_str());
					err += setsockopt(this->sock, SOL_SOCKET, TCP_LINGER2, &opt_int, sizeof(int));
					con << "Ajusta el linger2: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
					errno = 0;
				}
				if (opts_tcp->skepid != "" && opts_tcp->skepid != "0") {
					opt_int = atoi(opts_tcp->skepid.c_str());
					err += setsockopt(this->sock, IPPROTO_TCP, TCP_KEEPIDLE, &opt_int, sizeof(int));
					con << "Ajusta la inactividad de KeepAlive: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
					errno = 0;
				}
				if (opts_tcp->kaopt != "") {
					opt_int = atoi(opts_tcp->kaopt.c_str());
					err += setsockopt(this->sock, IPPROTO_TCP, TCP_KEEPINTVL, &opt_int, sizeof(int));
					con << "Ajusta el intervalo keepAlive: " << (err == 0 ? "correcto" : "error") << (errno != 0 ? strerror(errno) : "Sin problemas") << std::endl;
					errno = 0;
				}
				//Esta es la opción que venía originalmente en el ejemplo...
				opt_int = 1;
				err += setsockopt(this->sock, SOL_SOCKET, SO_REUSEADDR, &opt_int, sizeof(int));
				//FIN de las opciones de socket...
				err = connect(this->sock, p->ai_addr, p->ai_addrlen);
// 				fcntl(this->sock, F_SETFL, O_NONBLOCK);
				if (err != -1) {
					con << "Conexión exitosa!";
					retval = true;
				} else {
					con << "Error conectando la última fase: " << strerror(errno);
				}
			} else {
				con << "Error creando el socket!";
			}
		} else {
			con << "No encontró el destino";
		}
	} else {
		con << "Error transformando la dirección: " << gai_strerror(laDir);
	}
	milog->println(con.str(), 1);
	return retval;

	/*
	struct sockaddr_in serv_addr;
	con << "Intentando conectar a " << this->destino << ", puerto: " << this->puerto << std::endl;
	if ((this->sock = socket(AF_INET, SOCK_STREAM, 0)) >= 0) {
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(this->puerto);
		if(inet_pton(AF_INET, this->destino.c_str(), &serv_addr.sin_addr) > 0) {
			do {
				if (connect(this->sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) >= 0) {
					fcntl(this->sock, F_SETFL, O_NONBLOCK);
					//La conexión está viva...
					con << "Conexión exitosa!";
					retval = true;
				} else {
					con << "ERROR: Conexion fallida! ("<< errno <<") " << strerror(errno) << ". Reintento: " << retr << std::endl;
					sleep(1);
				}
			} while (!retval && ++retr < MAX_REINTENTOS_CLI);
		} else {
			con << "ERROR: Direccion invalida o no soportada";
		}
	} else {
		con << "ERROR: Error al crear socket";
	}
	return retval;
	*/
}

bool ClienteSocket::saluda(const std::__cxx11::string nombre, const uint8_t largoID) {
	stringstream con;
	con << "Enviando saludo con " << nombre << "(" << nombre.length() << ") y el largo de ID " << (int)largoID;
	unsigned tamMsg = 2;
	tamMsg += nombre.length();
	char buf[tamMsg];
	buf[0] = 0x00;
	buf[1] = largoID;
	memcpy(buf + 2, nombre.c_str(), nombre.length());
	this->milog->println(con.str(), 4);
	return (this->envia(buf, tamMsg) > 0);
}

bool ClienteSocket::nvoEquipo(const std::__cxx11::string idEquipo) {
	unsigned tamMsg = 1;
	tamMsg += idEquipo.length();
	char buf[tamMsg];
	buf[0] = 0x01;
	memcpy(buf + 1, idEquipo.c_str(), idEquipo.length());
	return (this->envia(buf, tamMsg) > 0);
}

ClienteSocket::Token * ClienteSocket::buscaToken(const char c, const uint8_t m, const bool i) {
	if (i) {
		//busca token de inicio...
		for (unsigned j = 0; j < this->toks.size(); j++) {
			if (this->toks[j]->inicio == c && ((this->toks[j]->inicio == this->toks[j]->fin && ((this->toks[j]->mascara & m) != this->toks[j]->mascara)) || (this->toks[j]->inicio != this->toks[j]->fin)))
				return this->toks.at(j);
		}
	} else {
		for (unsigned j = 0; j < this->toks.size(); j++) {
			if (this->toks[j]->fin == c && ((this->toks[j]->inicio == this->toks[j]->fin && ((this->toks[j]->mascara & m) == this->toks[j]->mascara)) || (this->toks[j]->inicio != this->toks[j]->fin)))
				return this->toks.at(j);
		}
	}
	return 0L;
}

bool ClienteSocket::actualizaEquipo(std::__cxx11::string consulta) {
	//Esta función hace la transformación de datos desde los querys de registro...
	stringstream campos, cpaso, con;
	uint8_t mskActiva = 0x00;
	Token * tk = 0L;
	unsigned nivel = 0;
	bool pos = false;
	char busca[] = "geomfromtext('POINT(";
	vector < string > valores;
	string idEquipo;
	double lat, lng;
	uint16_t vel, curso;
	uint8_t gsm, gps, j = 0;
	uint64_t edo;
	milog->println("Actualizando con:\n" + consulta, 4);
	consulta = consulta.substr(consulta.find("(") + 1);
	for (unsigned i = 0; i < consulta.length(); i++) {
		if ((tk = this->buscaToken(consulta[i], mskActiva, true)) != 0L) {
			mskActiva |= tk->mascara;
			nivel++;
		} else if ((tk = this->buscaToken(consulta[i], mskActiva, false)) != 0L) {
			mskActiva &= ~tk->mascara;
			nivel--;
		}
		if (consulta[i] == ',' && nivel == 0) {
			valores.push_back(campos.str());
			campos.str("");
		} else {
			campos << consulta[i];
		}
	}
	if (!campos.str().empty()) valores.push_back(campos.str());
	for (unsigned i = 0; i < valores.at(5).length(); i++) {
		if (!pos && valores.at(5)[i] == busca[0]) {
			pos = true;
			do {
				pos = pos && (valores.at(5)[i++] == busca[j++]);
			} while (pos && j < 20);
		} 
		if (pos) {
			do {
				cpaso << valores.at(5)[i++];
			} while (valores.at(5)[i] != 0x20);
			lng = atof(cpaso.str().c_str());
			cpaso.str("");
			i += 1;
			do {
				cpaso << valores.at(5)[i++];
			} while (valores.at(5)[i] != ')');
			lat = atof(cpaso.str().c_str());
			cpaso.str("");
			campos << "'laposicion',";
			pos = false;
		}
	}
	limpia(&valores.at(0));
	limpia(&valores.at(9));
	limpia(&valores.at(8));
	limpia(&valores.at(6));
	limpia(&valores.at(7));
	limpia(&valores.at(12));
	idEquipo = valores.at(0);
	gsm = atoi(valores.at(9).c_str());
	gps = atoi(valores.at(8).c_str());
	vel = atoi(valores.at(6).c_str());
	curso = atoi(valores.at(7).c_str());
	edo = atol(valores.at(12).c_str());
	milog->println("=== actualizaEquipo(str): La consulta ya fue procesada...", 4);
	return this->actualizaEquipo(idEquipo, lat, lng, vel, curso, gsm, gps, edo);
	/*Extraer longitud y latitud...
	CREATE PROCEDURE registraEvento(
		elIMEI char(15), 			0
		elTipo int, 				1
		elMomento datetime, 		2
		elVoltaje decimal(5,2), 	3
		losExtra varchar(500), 		4
		laPos point, 				5
		laVelocidad decimal(12,4), 	6
		elCurso decimal(6,3), 		7
		losSat int, 				8
		gsm tinyint, 				9
		pd decimal(7,2), 			10
		vd decimal(5,2), 			11
		elEstado bigint)			12
	*/
}


bool ClienteSocket::actualizaEquipo(const std::__cxx11::string idEquipo, const double lat, const double lng, const uint16_t vel, const uint16_t curso, const uint8_t gsm, const uint8_t gps, const uint64_t estado) {
	unsigned tamMsg = 22 + idEquipo.length();	//1 + 8 + 2 + 2 + 1 + 8 + el ID del equipo
	char buf[tamMsg];	//char * buf;
	uint8_t pos = 0;
	union {
		int		elEntero;
		char	losBytes[4];
	} latProc, lngProc;
	union {
		uint64_t	elEntero;
		char		losBytes[8];
	} statProc;
	if (!(lat == 0.0 || lng == 0.0)) {
		buf[pos++] = 0x02;
		//Agregar el ID...
		memcpy(buf + pos, idEquipo.c_str(), idEquipo.length());
		pos += idEquipo.length();
		latProc.elEntero = lat * 10000000;
		lngProc.elEntero = lng * 10000000;
		statProc.elEntero = estado;
		memcpy(buf + pos, latProc.losBytes, 4); pos += 4;
		memcpy(buf + pos, lngProc.losBytes, 4); pos += 4;
		buf[pos++] = (uint8_t)vel >> 8;
		buf[pos++] = (uint8_t)vel & 0x00FF;
		buf[pos++] = (uint8_t)curso >> 8;
		buf[pos++] = (uint8_t)curso & 0x00FF;
		buf[pos++] = (gps << 4) | (gsm & 0b00001111);
		memcpy(buf + pos, statProc.losBytes, 8);
		return (this->envia(buf, tamMsg) > 0);
	} else {
		//No enviamos actualizaciones sin posición...
		milog->println("Evitando enviar una actuallización sin posición válida", 2);
		return false;
	}
}

bool ClienteSocket::alerta(const std::__cxx11::string idEquipo) {
	unsigned tamMsg = 1;
	tamMsg += idEquipo.length();
	char buf[tamMsg];
	buf[0] = 0x03;
	memcpy(buf + 1, idEquipo.c_str(), idEquipo.length());
	return (this->envia(buf, tamMsg) > 0);
}

bool ClienteSocket::saleEquipo(const std::__cxx11::string idEquipo) {
	unsigned tamMsg = 1;
	tamMsg += idEquipo.length();
	char buf[tamMsg];
	buf[0] = 0x04;
	memcpy(buf + 1, idEquipo.c_str(), idEquipo.length());
	return (this->envia(buf, tamMsg) > 0);
}

int ClienteSocket::envia(const char * paquete, const unsigned tamMsg) {
	//Aquí vamos a agregar las partes de protocolo...
// 	this->milog->println("Paquete:       " + hex2str((unsigned char *)paquete, tamMsg, false), 1);
	unsigned pos = 0;
	if (this->fifando) {
		char buff[tamMsg + 9];
		buff[pos++] = 0x00;	//0
		buff[pos++] = 0x00;	//0
		buff[pos++] = 0x52;	//R
		buff[pos++] = 0x4E;	//N
		buff[pos++] = tamMsg;
		memcpy(buff + pos, paquete, tamMsg); pos += tamMsg;
		buff[pos++] = 0xFF;
		buff[pos++] = 0xFF;
		buff[pos++] = 0x52;
		buff[pos++] = 0x4E;
// 		this->milog->println(hex2str((unsigned char *)buff, tamMsg + 9, false), 1);
		return send(this->sock, buff, pos, 0);
	} else {
		this->milog->println("Falló el envío del paquete, está desconectado!", 2);
	}
	return -3;
}

void ClienteSocket::inicia() {
	this->fifando = true;
	this->milog->println("Inicia el hilo de escucha interno.", 1);
	this->proc = new thread(ClienteSocket::thProceso, this);
	this->proc->detach();
}

bool ClienteSocket::leeMensajes() {
	//Definición del protocolo hacia el cliente...
	bool retval = true;
	uint8_t terminador[4];
	uint16_t msgLen = 0;
	stringstream con;
	int err;
	memset(this->buff, 0x0, MAX_MENSAJE_CLI);
	string cmd = "", ideq = "";
	this->milog->println("=== leeMensajes(): Esperando mensajes desde el cliente, modo bloqueo...", 1);
	err = this->leeSocket(this->buff, 5);
	if (err == 5 && (this->buff[0] == 0xFF && this->buff[1] == 0xFF && this->buff[2] == 0x52 && this->buff[3] == 0x4E)) {
		msgLen = this->buff[4];
// 		con << "Tamaño del mensaje: " << msgLen;
		if ((err = this->leeSocket(this->buff, msgLen)) == msgLen) {
// 			con << " mensaje:  " << hex2str(this->buff, msgLen, false) << std::endl;
			if ((err = this->leeSocket(terminador, 4)) == 4) {
// 				con << "... leyó " << err << " bytes para el terminador: " << hex2str(terminador, err, false) << std::endl;
				if (terminador[0] == 0x00 && terminador[1] == 0x00 && terminador[2] == 0x52 && terminador[3] == 0x4E) {
					switch (this->buff[0]) {
						case 0xF0: //0xF0 - Enviar un comando a un equipo...
							//Extraer el mensaje...
							ideq = this->extraeStr(1, LARGO_ID);
							cmd = this->extraeStr(LARGO_ID + 1, msgLen - LARGO_ID);
							con << "Listos los valores: " << ideq << ", " << cmd << std::endl;
							this->milog->println(con.str(), 4); con.str("");
							if (this->propietario->envia(cmd, "websockets", ideq, DEST_EXTERNO, USR_PROTOCOLO)) {
								con << "Comando enviado!\n";
							}
							break;
						default:
							retval = false;
							break;
					}
				} else {
					con << "Paquete inválido, terminador incorrecto!!";
				}
			} else {
				con << "No hay bytes suficientes para el terminador!!";
			}
		} else {
			con << "Paquete incompleto, no pudo leer suficientes bytes. Leyó " << err << "("<< strerror(errno) << ")";
			retval = false;
		}
	} else if (err < 0) {
		sleep(1);
	} else {
		con << "=== leeMensajes(): Bytes leídos: " << err << " y el encabezado puede estar comprometido\n";
		con << "Error detectado: " << strerror(errno) << "\n";
		sleep(1);
		retval = false;
	}
	usleep(250 * 1000);
	if (!con.str().empty()) {
		this->milog->println(con.str(), 4);
	}
	return retval;
}

bool ClienteSocket::respuestaCmd(const std::__cxx11::string idEquipo, const std::__cxx11::string msg) {
	unsigned tamMsg = 1;
	unsigned pos = 1;
	tamMsg += idEquipo.length();
	tamMsg += msg.length();
	char buf[tamMsg];
	buf[0] = 0x05;
	memcpy(buf + pos, idEquipo.c_str(), idEquipo.length()); pos += idEquipo.length();
	memcpy(buf + pos, msg.c_str(), msg.length());
	return (this->envia(buf, tamMsg) > 0);
}


int ClienteSocket::leeSocket(uint8_t * buffer, unsigned bytes) {
	int retval = -2;
	int leidos = 0, reintentos = 0;
	int paso = 0;
	stringstream con;
	do {
// 		if (retval != -2) {
// 			usleep(250 * 1000);
// 		}
		errno = 0;
		retval = recv(this->sock, buffer + leidos, (bytes - leidos), MSG_WAITALL);
// 		con << "Intentando leer " << bytes << ", obtuvo " << retval << " y generó el error: " << strerror(errno) << std::endl;
		if (retval > 0) {
			leidos += retval;
		} else if (errno != EAGAIN) {
// 			con << "\nEn corto!! regresa " << retval << " despues de "<< reintentos << " intentos error: " << strerror(errno) << " y leídos: " << leidos;
			milog->println(con.str(), 4);
			return retval;
		}
		if ((++reintentos > MAX_REINTENTOS_CLI && (retval == -1 || errno == EAGAIN))) {
			leidos = -99;	//Posíblemente quiera decir que no hay datos...
			break;
		}
	} while (bytes != MAX_MENSAJE && (bytes - leidos) > 0);
// 	con << "\nRegresando: " << retval << " despues de "<< reintentos << " intentos error: " << strerror(errno) << " y leídos: " << leidos;
	milog->println(con.str(), 4);
	return retval;
}

void ClienteSocket::thProceso(ClienteSocket * obj) {
	obj->milog->println("=== thProceso(): inicia el hilo de escucha...", 4);
	while (obj->leeMensajes() && obj->fifando) {
		;
	}
	obj->termina();
}

void ClienteSocket::termina() {
	this->milog->println("=== termina(): Terminando la instancia del cliente interno", 1);
	this->fifando = false;
}

std::__cxx11::string ClienteSocket::extraeStr(const unsigned int pos, const unsigned largo) {
	char paso[largo];
	memset(paso, 0x00, largo);
	memcpy(paso, (char*)this->buff + pos, largo);
	return string(paso);
}

bool ClienteSocket::estaFifando() {
	return this->fifando;
}

ClienteSocket::~ClienteSocket() {
	close(this->sock);
	if (this->proc != 0L && this->proc->joinable()) this->proc->join();
}

