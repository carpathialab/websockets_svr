#ifndef __MITOTERO__
#define __MITOTERO__ 0

#include <iostream>
#include <string>
#include <vector>
#include "cliente.h"
#include "../superclases/protocolo.h"
#include "../../iolib/IOLogger.h"

#define USR_PROTOCOLO 0x01
#define USR_INTERNO 0x02
#define DEST_INTERNO 0x04
#define DEST_EXTERNO 0x08
#define USR_WEBSOCKET 0x10

#if defined (_ECOSVR_)
#define LARGO_ID 3
#elif defined (_FA24_4GSVR_)
#define LARGO_ID 10
#elif defined (_ST600SVR_) || defined (_ST410SVR_) || defined (_ST300SVR_) || defined (_ST4340SVR_)
#define LARGO_ID 9
#elif defined (_ST200SVR_)
#define LARGO_ID 6
#else
#define LARGO_ID 15
#endif

using namespace std;

class Protocolo;

struct Mensaje {
	unsigned long	id;
	string			idEquipo;
	string			remite;
	string			contenido;
	double			momento;
// 	Mensaje(const unsigned long i, const string eq, const string cont, const string usr, const double m) {
// 		this->id = i;
// 		this->idEquipo = eq;
// 		this->contenido = cont;
// 		this->remite = usr;
// 		this->momento = m;
// 	}
};


class Mitotiani {
public:
	Mitotiani(const string svr, const string pto, IOLogger * l, const bool depura, const string nom, const OpcionesTCP * opts_tcp);
	~Mitotiani();
	void firma(Protocolo * prot);
	void desfirma(const int fd);
	void desfirma(const string id);
	/** envia
	 * @brief Envía un mensaje y busca el destinatario
	 * @param string Mensaje a enviar
	 * @param string Remitente
	 * @param string ID del equipo destinatario
	 * @param uint8_t Tipo de destinatario que debe recibir el mensaje.
	 * @param uint8_t Tipo de usuario que debe de considerarse en la búsqueda
	 * @return Verdadero si el envío fue exitoso
	 */
	bool envia(const string msg, const string remite, const string idEquipo, const uint8_t tipoDestino, const uint8_t tipoUsr);
	
	/** equipoConectado
	 * @brief Busca si un equipo está conectado o no a los servidores de protocolo
	 * @param string ID del equipo a buscar
	 * @return Verdadero si lo encuentra
	 */
	bool equipoConectado(const string idEquipo);
	
	/** listaClientes
	 * @brief Regresa el listado de clientes que están conectados, filtrado por tipo de cliente.
	 * @param uint8_t Tipo de cliente, usar las constantes
	 * @returns Un string con los clientes delimitados por barra |
	 */
	string listaClientes(const uint8_t tipo);
	
	/** listaAtendidos
	 * @brief Regresa la lista de equipos que atiende un protocolo en específico, puede usar como parámetro el nombre del protocolo y la IP opcionalente. Si no se proporciona la IP, se consideraran todos los protocolos con ese nombre.
	 * @param string ID del protocolo.
	 * @param string IP del cliente (opcional).
	 * @returns Un string separado con la información delimitada por barra |
	 */
	string listaAtendidos(const string id, const string ip);
	
	ClienteSocket	*getNotificador();
	
private:
	vector< Protocolo* >	usuarios;
	ClienteSocket			*cliente;
	IOLogger				*milog;
	const OpcionesTCP		*opts_tcp;
	string					svrInterno;
	string					ptoInterno;
	bool					depura;
	string					nombre;
	
	unsigned long ultID;
	//Funciones
#ifndef _WEBSOCKETSSVR_
	void conectaCliente();
#endif
	Protocolo * busca(const string idEquipo, const uint8_t td, const uint8_t tu);
};
/*
 *  {
		this->fd = fd;
		this->id = id;
		this->nombre = nombre;
		this->tipo = tipo;
	} */
#endif
