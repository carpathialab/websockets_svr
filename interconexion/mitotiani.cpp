#include "mitotiani.h"

Mitotiani::Mitotiani(const string svr, const string pto, IOLogger * l, const bool depura, const string nom, const OpcionesTCP * opts) {
	this->ultID = 0;
	this->milog = l;
	this->milog->println("Iniciando el mitotero para " + nom, 1);
	this->svrInterno = svr;
	this->ptoInterno = pto;
	this->depura = depura;
	this->nombre = nom;
	this->cliente = 0L;
	this->opts_tcp = opts;
#ifndef _WEBSOCKETSSVR_
	//this->conectaCliente();
#endif
}

Mitotiani::~Mitotiani() {
#ifndef _WEBSOCKETSSVR_
	cliente->termina();
	delete cliente;
#endif
}

#ifndef _WEBSOCKETSSVR_
void Mitotiani::conectaCliente() {
	if (this->cliente != 0L && !this->cliente->estaFifando()) {
		delete this->cliente;
		this->cliente = 0L;
	}
	if (this->cliente == 0L) {
		this->cliente = new ClienteSocket(this->ptoInterno, this->svrInterno, this->milog, this, this->depura, this->opts_tcp);
		if (this->cliente->estaFifando()) {
			if (this->cliente->saluda(this->nombre, LARGO_ID)) {
				this->milog->println("Saludó correctamente al servidor interno", 1);
			}
		}
	}
}
#endif

void Mitotiani::firma(Protocolo* prot) {
	prot->setConcentradora(this);
	this->usuarios.push_back(prot);
}

bool Mitotiani::envia(const std::__cxx11::string msg, const std::__cxx11::string remite, const std::__cxx11::string idEquipo, const uint8_t tipoDestino, const uint8_t tipoUsr) {
	bool retval = false;
	stringstream con;
	for (unsigned i = 0; i < this->usuarios.size(); i++) {
		con << i << std::endl;
		if (this->usuarios.at(i)->atiende(idEquipo, tipoDestino, tipoUsr)) {
			Protocolo::Mensaje * m = new Protocolo::Mensaje(++this->ultID, idEquipo, msg, remite, 0);
			retval = retval || this->usuarios.at(i)->enviaComando(m);
			delete m;
		}
	}
	if (!retval) con << "--- envia(): No encontró destinatarios para enviar " << msg << " al destino " << idEquipo;
	this->milog->println(con.str(), 4);
	return retval;
}

Protocolo * Mitotiani::busca(const std::__cxx11::string idEquipo, const uint8_t td, const uint8_t tu) {
	for (unsigned i = 0; i < this->usuarios.size(); i++) {
		if (this->usuarios.at(i)->atiende(idEquipo, td, tu)) return this->usuarios.at(i);
	}
	return 0L;
}

void Mitotiani::desfirma(const int fd) {
	for (unsigned i = 0; i < this->usuarios.size(); i++) {
		if (this->usuarios.at(i)->getFD() == fd) {
			this->usuarios.erase(this->usuarios.begin() + i);
			break;
		}
	}
}

void Mitotiani::desfirma(const std::__cxx11::string id) {
	for (unsigned i = 0; i < this->usuarios.size(); i++) {
		if (this->usuarios.at(i)->getIDEquipo() == id) {
			this->usuarios.erase(this->usuarios.begin() + i);
			break;
		}
	}
}

ClienteSocket * Mitotiani::getNotificador() {
	//Aquí vamos a poner el monitor...
#ifndef _WEBSOCKETSSVR_
	this->conectaCliente();
	return this->cliente;
#else
	return 0L;
#endif
}

bool Mitotiani::equipoConectado(const std::__cxx11::string idEquipo) {
	return (this->busca(idEquipo, DEST_INTERNO, USR_INTERNO) != 0L);
}

std::__cxx11::string Mitotiani::listaClientes(const uint8_t tipo) {
	stringstream retval;
	for (unsigned i = 0; i < this->usuarios.size(); i++) {
		if (this->usuarios.at(i)->getTipo() == tipo) retval << this->usuarios.at(i)->getIDEquipo() << ". IP: " << this->usuarios.at(i)->getIPCliente() << "|";
	}
	return retval.str();
}

std::__cxx11::string Mitotiani::listaAtendidos(const std::__cxx11::string id, const std::__cxx11::string ip) {
	stringstream retval;
	for (unsigned i = 0; i < this->usuarios.size(); i++) {
		if (this->usuarios.at(i)->getIDEquipo() == id && (ip != "" ? (this->usuarios.at(i)->getIPCliente() == ip) : true)) {
			retval << this->usuarios.at(i)->getIDEquipo() << ":" << this->usuarios.at(i)->getIPCliente() << ". " << this->usuarios.at(i)->dimeAtendidos() << "|";
		}
	}
	return retval.str();
}
