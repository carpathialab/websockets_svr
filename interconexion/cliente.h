#ifndef __CLIENTE__
#define __CLIENTE__ 0
#define MAX_MENSAJE_CLI 1024
#define MAX_REINTENTOS_CLI 20

#include <string>
#include <sstream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <thread>
#include <fcntl.h>
#include <netdb.h>

//Librerías y ayudas propias
#include "../../iolib/IOLogger.h"
#include "../superclases/utilerias.h"
#include "../interconexion/mitotiani.h"

using namespace std;
using namespace IOCore;

class Mitotiani;

class ClienteSocket {
public:
	ClienteSocket(const string p, const string ip, IOLogger * log, Mitotiani * prop, bool depura, const OpcionesTCP * opts);
	/** saluda
	 * @brief Envía el dato de identificación de este protocolo hacia el servidor de websockets
	 * @param string Nombre con el que se va a identificar
	 * @param uint8_t El largo en bytes que miden los identificadores
	 * @returns Verdadero si logró enviar correctamente el mensaje
	 */
	bool saluda(const string nombre, const uint8_t largoID);
	
	/** nvoEquipo
	 * @brief registra la presencia de un nuevo equipo conectado al servidor
	 * @param string ID del equipo que se acaba de conectar
	 * @returns Verdadero si el mensaje fue enviado correctamente
	 */
	bool nvoEquipo(const string idEquipo);
	
	
	bool actualizaEquipo(const string idEquipo, const double lat, const double lng, const uint16_t vel, const uint16_t curso, const uint8_t gsm, const uint8_t gps, const uint64_t estado);
	bool actualizaEquipo(const string consulta);
	bool alerta(const string idEquipo);
	bool respuestaCmd(const string idEquipo, const string msg);
	bool saleEquipo(const string idEquipo);
	void termina();
	bool leeMensajes();
	bool estaFifando();
	~ClienteSocket();

private:
	struct Token {
		char inicio;
		char fin;
		uint8_t mascara;
		Token(const char i, const char f, const uint8_t msk);
	};
// 	uint16_t	puerto;
	string		puerto;
	string		destino;
	//Opciones del socket ajustables
// 	string		errsock, 
// 				kaopt, 
// 				soka, 
// 				soto, 
// 				slin, 
// 				slin2, 
// 				skepid;
	const OpcionesTCP	*opts_tcp;
	IOLogger	*milog;
	int			sock;
	bool		fifando, modoDebug;
	thread		*proc;
	Mitotiani	*propietario;
	uint8_t		buff[MAX_MENSAJE_CLI];
	
	vector < Token * > toks;

	//Funciones;
	bool contecta();
	void inicia();
	int envia(const char * paquete, const unsigned tamMsg);
	int leeSocket(uint8_t * buffer, unsigned bytes);
	static void thProceso(ClienteSocket * obj);
	string extraeStr(const unsigned pos, const unsigned largo);
	Token * buscaToken(const char c, const uint8_t m, const bool i);
};
#endif


/*
 * keepalive=1
enviotimeout=30
linger=0
linger2=30
inactividadka=60
###erroressocket=1      //ATENCIÓN: Opción de solo lectura
intervalokeepalive=5
*/
