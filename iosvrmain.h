#pragma once
/**
 * Operación básica de cadenas, etc...
 */
#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <string.h>
#include <sstream>
#include <sys/file.h>
#include <bitset>

/**
 * Necesario para el manejo de threads...
 */
// #include <pthread.h>
#include <thread>
/**
 * Conexiones TCP, encabezados, etc...
 */
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <fcntl.h>
/**
 * Manejo de errores, señales e interfaz con el SO
 */
#include <signal.h>
#include <execinfo.h>
#include <stdexcept>
#include <errno.h>
/**
 * Librerías propias y privadas...
 */
#include "../iolib/IOCoreFuncs.h"
#include "../iolib/ioconfigurador.h"
#include "../iolib/IOLogger.h"
#include "usuarios.h"
#include "./superclases/protocolo.h"
#include "DBConexion.h"
#include "estadoman.h"
#include "superclases/crc.h"
#include "./interconexion/mitotiani.h"
#include "./superclases/servidor.h"
#include "./superclases/utilerias.h"

#ifdef _ECOSVR_
#include "./protocolos/eco.h"
#endif
#ifdef _QBITSVR_
#include "./protocolos/qbit.h"
#endif
#ifdef _ST600SVR_
#include "./protocolos/st600.h"
#endif
#ifdef _ST410SVR_
#include "./protocolos/st410g.h"
#endif
#ifdef _ST300SVR_
#include "./protocolos/st300.h"
#endif
#ifdef _ST200SVR_
#include "./protocolos/st200.h"
#endif
#ifdef _ST4340SVR_
#include "./protocolos/st4340.h"
#endif
#ifdef _FA24_4GSVR_
#include "./protocolos/fa28_4g.h"
#endif
#ifdef _MANTENIMIENTOSVR_
#include "./protocolos/mantenimiento.h"
#endif
#ifdef _TELTONIKASVR_
#include "./protocolos/teltonika.h"
#endif
#ifdef _GT06E_
#include "./protocolos/gt06e.h"
#endif
#ifdef _GT08_
#include "./protocolos/gt08.h"
#endif
#ifdef _SP1600_
#include "./protocolos/sp1600.h"
#endif
#ifdef _QUECLINK_
#include "./protocolos/queclink.h"
#endif
#ifdef _WEBSOCKETSSVR_
#include "./protocolos/websockets.h"
#define LATENCIA_MSG 30.0
#else
#define LATENCIA_MSG 7200
#endif

#define MAX_ERRORES_LLENO 7
#define LATENCIA_SIG 7

using namespace std;
using namespace IOCore;
using namespace sql;

//funciones de servidor:
int iniciaEscucha();
int svrConecta(int svr_fd, string *ipcliente);
int envia(int fd, string datos);
int envia(string cliente, string datos);

void actualizaDespas(const string id, const string datos);
void notifica(const string id, const string sujeto, const string nvoEstado, const bool esOper);
void *svrLector(void *arg);
void *santo(void *arg);		//Thread para mantener vivos los sockets...
void cicloPrincipal(int svr_fd);
void bestias(int elFD);
void liberaTodos();
//Manejador de señales...
void manejaSignals(int numSignal);
void registraNoProc(const string mensaje, const vector< string > partes);
void imprimeBufBinario(const char*, const unsigned int);
//Funciones genéricas
static inline uint32_t murmur_32_scramble(uint32_t k);
uint32_t murmur3_32(const uint8_t* key, size_t len, uint32_t seed);
void hazMagia();

Protocolo * generaProtocolo(int elFD);

//Variables...
DBConexion	* dbman;
string		svrnombre;
string		pid;
string		imeiDebug;
ulong		regInit;
ulong		regFin;
unsigned	erroresLleno;
bool		dbAplica;
bool		hazPausas;
EstadoMan	* edoMan;
bool		terminando;
char		leidoEstado;
// vector < Protocolo::cmdEnviar* > listaEnvios;
// vector < Protocolo::cmdEnviar* > cmdPendientes;
double		momento_sig;
